package players;

import java.awt.Color;
import java.util.ArrayList;

import blocks.LandProperty;
import blocks.Property;
import cards.TitleDeedCard;
import gameControl.GameBoard.ColorGroup;
import gameControl.GameBoard.BuildingType;

/**
 * Game player
 * 
 * @author Rui
 *
 */
public class GamePlayer implements Player {
	private int id, balance, cardId, position, lastPosition, counterForTwo;
	private String name;
	private Color color;
	private ArrayList<Property> properties;
	private ArrayList<ColorGroup> colorGroups;
	private boolean isPrisoned, isRetired;
	private boolean hasChanceFreeCard, hasCCFreeCard, freeCardLock, hasPaidFine, hasPaidRent;
	private int[] lastTwoRolls;

	public GamePlayer() {
		this.balance = 2000;
		this.cardId = -1;
		this.position = 0;
		this.properties = new ArrayList<>();
		this.colorGroups = new ArrayList<>();
		this.isPrisoned = false;
		this.isRetired = false;
		this.hasChanceFreeCard = false;
		this.hasCCFreeCard = false;
		this.freeCardLock = false;
		this.hasPaidFine = false;
		this.hasPaidFine = false;
		this.lastTwoRolls = new int[2];
		lastTwoRolls[0] = -1;
		lastTwoRolls[1] = -1;
		this.counterForTwo = 0;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setColor(Color color) {
		this.color = color;
	}

	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public boolean isRetired() {
		return isRetired;
	}

	public void retire(boolean retire) {
		isRetired = retire;
		for (Property p : properties) {
			if (p instanceof LandProperty) {
				for (int i = 0; i < ((LandProperty) p).getBuildings().size(); i++)
					((LandProperty) p).sellHouse();
			}
			p.setOwner(null);
		}
		while (hasFreeCard())
			useFreeCard();
	}

	/**
	 * Get the last two rolls of the player.
	 * 
	 * @return last two rolls
	 */
	public int[] getLastTwoRolls() {
		return lastTwoRolls;
	}

	/**
	 * Set the last roll of the player.
	 * 
	 * @param roll last roll
	 */
	public void setLastRoll(int roll) {
		lastTwoRolls[counterForTwo] = roll;
		counterForTwo = (counterForTwo + 1) % 2;
	}

	/**
	 * Change the imprisoning state of the player.
	 * <p>
	 * Clear the buffer for last two rolls.
	 * <p>
	 * Reset the flag for paying the fine.
	 */
	public void changeImprisonState() {
		isPrisoned = !isPrisoned;
		lastTwoRolls = new int[2];
		lastTwoRolls[0] = -1;
		lastTwoRolls[1] = -1;
		hasPaidFine = false;
	}

	/**
	 * Determine whether the player is imprisoned.
	 * 
	 * @return true if is imprisoned
	 */
	public boolean isImprisoned() {
		return isPrisoned;
	}

	/**
	 * Obtain one 'Get Out of Jail Free' card.
	 * 
	 * @param cardId
	 */
	public void obtainFreeCard(int cardId) {
		if (cardId == 6)
			hasChanceFreeCard = true;
		else if (cardId == 4)
			hasCCFreeCard = true;
	}

	/**
	 * Use one 'Get Out of Jail Free' card.
	 * 
	 * @return card id
	 */
	public int useFreeCard() {
		if (!hasChanceFreeCard && hasCCFreeCard) {
			hasCCFreeCard = false;
			return 4;
		} else if (hasChanceFreeCard)
			hasChanceFreeCard = false;
		return 6;
	}

	/**
	 * Determine whether the player has 'Get Out of Jail Free' card.
	 * 
	 * @return true if has the card
	 */
	public boolean hasFreeCard() {
		return hasChanceFreeCard || hasCCFreeCard;
	}

	/**
	 * Change the availability of the 'Get Out of Jail Free' card.
	 */
	public void freeCardLock() {
		freeCardLock = !freeCardLock;
	}

	/**
	 * Determine whether the 'Get Out of Jail Free' card is available.
	 * 
	 * @return true if not locked
	 */
	public boolean isFreeCardLocked() {
		return freeCardLock;
	}

	/**
	 * Set the flag for paying fine true.
	 */
	public void payFine() {
		hasPaidFine = true;
	}

	/**
	 * Determine whether the player has paid fine.
	 * 
	 * @return true if paid fine
	 */
	public boolean hasPaidFine() {
		return hasPaidFine;
	}

	/**
	 * Set the flag for paying rent.
	 */
	public void payRent() {
		hasPaidRent = true;
	}

	/**
	 * Determine whether the player has paid rent.
	 * 
	 * @return true if paid rent
	 */
	public boolean hasPaidRent() {
		return hasPaidRent;
	}

	/**
	 * Set the id of the obtained card.
	 * 
	 * @param id
	 */
	public void drawCard(int id) {
		cardId = id;
	}

	/**
	 * Get the id of the obtained card.
	 * 
	 * @return card id
	 */
	public int getCardId() {
		return cardId;
	}

	/**
	 * Determine whether the player has drawn card.
	 * 
	 * @return true if has drawn a card
	 */
	public boolean hasDrawnCard() {
		return cardId >= 0;
	}

	/**
	 * Move the player forward for specified steps.
	 * 
	 * @param numOfSteps number of steps
	 */
	public void move(int numOfSteps) {
		lastPosition = position;
		lastTwoRolls[counterForTwo] = numOfSteps;
		counterForTwo = (counterForTwo + 1) % 2;
		position = (position + numOfSteps) % 40;
		hasPaidRent = false;
		cardId = -1;
	}

	/**
	 * Move the player to the specified position.
	 * 
	 * @param pos position
	 */
	public void moveTo(int pos) {
		lastPosition = position;
		position = pos;
	}

	/**
	 * Get the position of the player.
	 * 
	 * @return position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Get the last position of the player.
	 * 
	 * @return last position
	 */
	public int getLastPosition() {
		return lastPosition;
	}

	/**
	 * Change the balance of the player.
	 * 
	 * @param income
	 */
	public void income(int income) {
		balance += income;
	}

	/**
	 * Get the balance of the player.
	 * 
	 * @return balance
	 */
	public int getBalance() {
		return balance;
	}

	/**
	 * Get the color groups of the owned properties.
	 * 
	 * @return color groups
	 */
	public ArrayList<ColorGroup> getColorGroups() {
		return colorGroups;
	}

	/**
	 * Get all the owned properties.
	 * 
	 * @return properties
	 */
	public ArrayList<Property> getProperties() {
		return properties;
	}

	/**
	 * Get the owned properties in a specified color group.
	 * 
	 * @param color color group
	 * @return list of properties
	 */
	public ArrayList<Property> getPropertiesInGroup(ColorGroup color) {
		ArrayList<Property> properties = new ArrayList<>();
		if (color == ColorGroup.BLACK) {
			for (Property p : this.properties) {
				if (p.getPosition() % 5 == 0)
					properties.add(p);
			}
		} else if (color == ColorGroup.WHITE) {
			for (Property p : this.properties) {
				if (p.getPosition() == 12 || p.getPosition() == 28)
					properties.add(p);
			}
		} else {
			for (Property p : this.properties) {
				if (p instanceof LandProperty && ((LandProperty) p).getColorGroup() == color)
					properties.add((LandProperty) p);
			}
		}
		return properties;
	}

	/**
	 * Buy a property.
	 * <p>
	 * Update the list of color groups if the property is the first one that is
	 * owned in the group.
	 * 
	 * @param property
	 */
	public void buyProperty(Property property, int price) {
		properties.add(property);
		if (property instanceof LandProperty && !colorGroups.contains(((LandProperty) property).getColorGroup()))
			colorGroups.add(((LandProperty) property).getColorGroup());
		else if (!(property instanceof LandProperty)) {
			if (property.getPosition() % 5 == 0 && !colorGroups.contains(ColorGroup.BLACK))
				colorGroups.add(ColorGroup.BLACK);
			else if (property.getPosition() % 5 != 0 && !colorGroups.contains(ColorGroup.WHITE))
				colorGroups.add(ColorGroup.WHITE);
		}
		balance -= price;
	}

	/**
	 * Sell a property.
	 * <p>
	 * Remove the color group of the property if it is the only property owned in
	 * the group.
	 * 
	 * @param property
	 */
	public void sellProperty(Property property) {
		properties.remove(property);
		balance += property.getMortgage();
		if (property instanceof LandProperty) {
			for (Property p : properties) {
				if (p instanceof LandProperty
						&& ((LandProperty) p).getColorGroup() == ((LandProperty) property).getColorGroup())
					return;
			}
			colorGroups.remove(((LandProperty) property).getColorGroup());
		} else {
			for (Property p : properties) {
				if (!(p instanceof LandProperty)) {
					if (p.getPosition() % 5 == 0 && property.getPosition() % 5 == 0)
						return;
					else if (p.getPosition() % 5 != 0 && property.getPosition() % 5 != 0)
						return;
				}
			}
			if (property.getPosition() % 5 == 0)
				colorGroups.remove(ColorGroup.BLACK);
			else
				colorGroups.remove(ColorGroup.WHITE);
		}
	}

	@Override
	public int getPropertyValue() {
		int value = 0;
		for (Property p : properties) {
			value += p.getMortgage();
			if (p instanceof LandProperty)
				value += computeHousePrice((LandProperty) p);
		}
		return value;
	}

	/**
	 * Get the total house price of a property.
	 * 
	 * @param p constructible property
	 * @return house price
	 */
	private int computeHousePrice(LandProperty p) {
		int index = TitleDeedCard.descriptor(p.getPosition()).length;
		int price = (int) TitleDeedCard.descriptor(p.getPosition())[index - 2];
		int value = 0;
		for (BuildingType h : ((LandProperty) p).getBuildings()) {
			if (h == BuildingType.HOUSE)
				value += price;
			else
				value += (price * 5);
		}
		return value;
	}
}
