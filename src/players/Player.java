package players;

import java.awt.Color;

/**
 * Player interface
 * 
 * @author Rui
 *
 */
public interface Player {

	/**
	 * Set the id of the player.
	 * 
	 * @param id
	 */
	public void setId(int id);

	/**
	 * Get the id of the player.
	 * 
	 * @return id
	 */
	public int getId();

	/**
	 * Set the name of the player.
	 * 
	 * @param name
	 */
	public void setName(String name);

	/**
	 * Get the name of the player.
	 * 
	 * @return name
	 */
	public String getName();

	/**
	 * Set the color of the player.
	 * 
	 * @param color
	 */
	public void setColor(Color color);

	/**
	 * Get the color of the player.
	 * 
	 * @return color
	 */
	public Color getColor();

	/**
	 * Determine whether the player is retired.
	 * 
	 * @return true if the player is retired
	 */
	public boolean isRetired();

	/**
	 * Get the total value of the player's property.
	 * 
	 * @return value
	 */
	public int getPropertyValue();

}
