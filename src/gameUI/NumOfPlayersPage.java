package gameUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Welcome page.
 * <p>
 * Select number of players.
 * 
 * @author Rui
 *
 */
public class NumOfPlayersPage {
	private static final int WIDTH = 1850;
	private static final int HEIGHT = 925;
	private Font titleFont, labelFont, buttonFont;
	private int numOfPlayers = 2;
	private static final Integer[] OPTIONS = { 2, 3, 4, 5, 6, 7, 8, 9 };

	public NumOfPlayersPage() {
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		p1.setLayout(new GridLayout(2, 1));
		p2.setLayout(new GridLayout(2, 1));
		p3.setLayout(new GridLayout(2, 1));
		p4.setLayout(new GridLayout(1, 7));
		p1.setPreferredSize(new Dimension(WIDTH, HEIGHT));

		titleFont = new Font("title", Font.BOLD, 60);
		labelFont = new Font("label", Font.ROMAN_BASELINE, 20);
		buttonFont = new Font("button", Font.BOLD, 28);

		JLabel str = new JLabel("<html>Number of Players</html>");
		str.setFont(labelFont);
		str.setHorizontalAlignment(JLabel.CENTER);
		str.setBackground(Color.WHITE);
		str.setOpaque(true);
		str.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, false));
		JComboBox<Integer> comboBox = new JComboBox<>(OPTIONS);
		comboBox.setFont(labelFont);
		comboBox.setBackground(Color.WHITE);
		JButton button = new JButton("<html>START!</html>");
		button.setFont(buttonFont);
		button.setBackground(Color.WHITE);

		p4.add(new JPanel());
		p4.add(new JPanel());
		p4.add(str);
		p4.add(comboBox);
		p4.add(button);
		p4.add(new JPanel());
		p4.add(new JPanel());

		p3.add(p4);
		p3.add(new JPanel());

		p2.add(p3);
		p2.add(new JPanel());

		JLabel logo = new JLabel("<html>MONOPOLY</html>");
		logo.setFont(titleFont);
		logo.setHorizontalAlignment(JLabel.CENTER);

		p1.add(logo);
		p1.add(p2);

		JFrame frame = new JFrame("Monopoly");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(p1);
		frame.pack();

		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getActionCommand().equals("comboBoxChanged")) {
					numOfPlayers = (int) comboBox.getSelectedItem();
				}
			}

		});

		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				new GameBoardPage(numOfPlayers);
			}

		});
	}

	/**
	 * Show the starting page.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new NumOfPlayersPage();
	}

}
