package gameUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import blocks.Block;
import blocks.CardPoolBlock;
import blocks.Property;
import blocks.LandProperty;
import cards.Chance;
import cards.CommunityChest;
import gameControl.GameBoard;
import gameControl.GameBoard.ColorGroup;
import gameControl.GameControl;
import gameControl.GameBoard.BuildingType;
import players.GamePlayer;

/**
 * Game board page.
 * <p>
 * Game board on the left half panel.
 * <p>
 * Game control & info display on the right.
 * 
 * @author Rui
 *
 */
public class GameBoardPage {
	private static final int WIDTH = 1850;
	private static final int HEIGHT = 925;

	private JPanel mainPanel, mapPanel, propertyPanel, playerPanel;
	private ArrayList<JLabel> blocks, propertyDetails;
	private JLabel logo;
	private JLabel color, blockName, status, blockStatus, details, constructionDetails, p, price;
	private JLabel icon, playerName, b, balance, property, freeCard, numOfFreeCard, dice1, dice2;
	private JComboBox<ColorGroup> colorGroups;
	private ArrayList<JButton> sellProperty;
	private JButton blockAction1, blockAction2, playerAction, freeCardLock;
	private Font titleFont, labelFont, textFont, buttonFont;
	private Border borderC, borderR;
	private boolean endTurn;
	private int numOfPlayers, currentPlayerId;
	private ColorGroup currentGroup;

	public static GameControl control = new GameControl();

	public GameBoardPage(int numOfPlayers) {
		this.blocks = new ArrayList<>();
		this.propertyDetails = new ArrayList<>();
		this.sellProperty = new ArrayList<>();
		this.numOfPlayers = numOfPlayers;
		this.currentPlayerId = 0;
		this.endTurn = false;
		setupPage();
	}

	/**
	 * Page setup.
	 */
	private void setupPage() {
		control.setUpPlayers(numOfPlayers);

		mainPanel = new JPanel();
		mapPanel = new JPanel();
		propertyPanel = new JPanel();
		playerPanel = new JPanel();
		JPanel controlPanel = new JPanel();
		mainPanel.setLayout(new GridLayout(1, 2));
		mapPanel.setLayout(new BorderLayout());
		controlPanel.setLayout(new GridLayout(1, 2));
		propertyPanel.setLayout(new GridLayout(7, 1));
		playerPanel.setLayout(new GridLayout(7, 1));

		titleFont = new Font("title", Font.BOLD, 20);
		labelFont = new Font("label", Font.ROMAN_BASELINE, 18);
		textFont = new Font("text", Font.ROMAN_BASELINE, 12);
		buttonFont = new Font("button", Font.BOLD, 20);

		borderC = BorderFactory.createLineBorder(Color.BLACK, 2, false);
		borderR = BorderFactory.createEmptyBorder(2, 2, 2, 2);

		JPanel mp1 = new JPanel();
		JPanel mp2 = new JPanel();
		JPanel mp3 = new JPanel();
		JPanel mp4 = new JPanel();
		mp1.setLayout(new BorderLayout());
		mp2.setLayout(new BorderLayout());
		mp3.setLayout(new BorderLayout());
		mp4.setLayout(new BorderLayout());

		logo = new JLabel("<html>MONOPOLY</html>");
		logo.setFont(new Font("logo", Font.CENTER_BASELINE, 72));
		logo.setHorizontalAlignment(JLabel.CENTER);

		for (int i = 0; i < 40; i++) {
			JLabel b = new JLabel();
			ImageIcon icon = new ImageIcon(ClassLoader.getSystemResource("images/blocks/" + i + ".png"));
			if ((i > 0 && i < 10) || (i > 20 && i < 30))
				icon.setImage(icon.getImage().getScaledInstance(75, 125, Image.SCALE_DEFAULT));
			else if ((i > 10 && i < 20) || (i > 30 && i < 40))
				icon.setImage(icon.getImage().getScaledInstance(125, 75, Image.SCALE_DEFAULT));
			else
				icon.setImage(icon.getImage().getScaledInstance(125, 125, Image.SCALE_DEFAULT));
			b.setIcon(icon);
			b.setHorizontalAlignment(JLabel.CENTER);
			blocks.add(i, b);
		}

		ArrayList<Color> colors = new ArrayList<>();
		for (int i = 0; i < numOfPlayers; i++) {
			colors.add(control.getPlayers().get(i).getColor());
		}
		blocks.get(0).setBorder(compoundBorder(colors));

		JPanel mp1c = new JPanel();
		mp1c.setLayout(new GridLayout(1, 9));
		for (int i = 0; i <= 10; i++) {
			if (i == 0)
				mp1.add(blocks.get(i), "East");
			else if (i == 10)
				mp1.add(blocks.get(i), "West");
			else
				mp1c.add(blocks.get(10 - i));
		}
		mp1.add(mp1c, "Center");

		JPanel mp2c = new JPanel();
		mp2c.setLayout(new GridLayout(9, 1));
		for (int i = 11; i < 20; i++) {
			mp2c.add(blocks.get(30 - i));
		}
		mp2.add(mp2c, "Center");

		JPanel mp3c = new JPanel();
		mp3c.setLayout(new GridLayout(1, 9));
		for (int i = 20; i <= 30; i++) {
			if (i == 20)
				mp3.add(blocks.get(i), "West");
			else if (i == 30)
				mp3.add(blocks.get(i), "East");
			else
				mp3c.add(blocks.get(i));
		}
		mp3.add(mp3c, "Center");

		JPanel mp4c = new JPanel();
		mp4c.setLayout(new GridLayout(9, 1));
		for (int i = 31; i < 40; i++) {
			mp4c.add(blocks.get(i));
		}
		mp4.add(mp4c, "Center");

		mapPanel.add(mp1, "South");
		mapPanel.add(mp2, "West");
		mapPanel.add(mp3, "North");
		mapPanel.add(mp4, "East");
		mapPanel.add(logo, "Center");

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 2));
		color = new JLabel();
		color.setBackground(Color.WHITE);
		color.setOpaque(true);
		ImageIcon blockIcon = new ImageIcon(ClassLoader.getSystemResource("images/infoPanel/blockInfo/" + "go.png"));
		blockIcon.setImage(blockIcon.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
		color.setIcon(blockIcon);
		color.setHorizontalAlignment(JLabel.CENTER);
		blockName = new JLabel("<html>Go</html>");
		blockName.setFont(titleFont);
		blockName.setHorizontalAlignment(JLabel.CENTER);
		blockName.setBackground(Color.WHITE);
		blockName.setOpaque(true);
		p1.add(color);
		p1.add(blockName);
		p1.setBorder(borderR);

		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1, 2));
		status = new JLabel("");
		status.setFont(titleFont);
		status.setHorizontalAlignment(JLabel.CENTER);
		status.setBackground(Color.LIGHT_GRAY);
		status.setOpaque(true);
		blockStatus = new JLabel();
		blockStatus.setFont(labelFont);
		blockStatus.setHorizontalAlignment(JLabel.CENTER);
		blockStatus.setBackground(Color.WHITE);
		blockStatus.setOpaque(true);
		p2.add(status);
		p2.add(blockStatus);
		p2.setBorder(borderR);

		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(1, 2));
		details = new JLabel("");
		details.setFont(titleFont);
		details.setHorizontalAlignment(JLabel.CENTER);
		details.setBackground(Color.LIGHT_GRAY);
		details.setOpaque(true);
		constructionDetails = new JLabel();
		constructionDetails.setFont(labelFont);
		constructionDetails.setHorizontalAlignment(JLabel.CENTER);
		constructionDetails.setBackground(Color.WHITE);
		constructionDetails.setOpaque(true);
		p3.add(details);
		p3.add(constructionDetails);
		p3.setBorder(borderR);

		JPanel p4 = new JPanel();
		p4.setLayout(new GridLayout(1, 2));
		p = new JLabel("");
		p.setFont(titleFont);
		p.setHorizontalAlignment(JLabel.CENTER);
		p.setBackground(Color.LIGHT_GRAY);
		p.setOpaque(true);
		price = new JLabel();
		price.setFont(labelFont);
		price.setHorizontalAlignment(JLabel.CENTER);
		price.setBackground(Color.WHITE);
		price.setOpaque(true);
		p4.add(p);
		p4.add(price);
		p4.setBorder(borderR);

		JPanel p5 = new JPanel();
		JPanel p6 = new JPanel();

		JPanel p7 = new JPanel();
		p7.setLayout(new GridLayout(1, 2));
		blockAction1 = new JButton();
		blockAction1.setFont(buttonFont);
		blockAction1.setHorizontalAlignment(JLabel.CENTER);
		blockAction1.setBackground(Color.WHITE);
		blockAction1.setEnabled(false);
		blockAction2 = new JButton();
		blockAction2.setFont(buttonFont);
		blockAction2.setHorizontalAlignment(JLabel.CENTER);
		blockAction2.setBackground(Color.WHITE);
		blockAction2.setEnabled(false);
		p7.add(blockAction1);
		p7.add(blockAction2);
		p7.setBorder(borderR);

		propertyPanel.add(p1);
		propertyPanel.add(p2);
		propertyPanel.add(p3);
		propertyPanel.add(p4);
		propertyPanel.add(p5);
		propertyPanel.add(p6);
		propertyPanel.add(p7);
		propertyPanel.setBorder(borderC);

		blockAction1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updateBlockPanel(true);
				updatePlayerPanel(false);
			}

		});
		blockAction2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				AuctionHousePage page = new AuctionHousePage();
				page.addWindowListener(new WindowAdapter() {
					public void windowClosed(WindowEvent e) {
						super.windowClosed(e);
						updateBlockPanel(false);
						updatePlayerPanel(false);
					}
				});
			}

		});

		GamePlayer player = control.getPlayers().get(0);

		JPanel p8 = new JPanel();
		p8.setLayout(new GridLayout(1, 2));
		icon = new JLabel();
		icon.setHorizontalAlignment(JLabel.CENTER);
		icon.setBackground(player.getColor());
		icon.setOpaque(true);
		playerName = new JLabel("<html>" + player.getName() + "</html>");
		playerName.setFont(titleFont);
		playerName.setHorizontalAlignment(JLabel.CENTER);
		playerName.setBackground(Color.WHITE);
		playerName.setOpaque(true);
		p8.add(icon);
		p8.add(playerName);
		p8.setBorder(borderR);

		JPanel p9 = new JPanel();
		p9.setLayout(new GridLayout(1, 2));
		b = new JLabel("<html>Balance</html>");
		b.setFont(titleFont);
		b.setHorizontalAlignment(JLabel.CENTER);
		b.setBackground(Color.LIGHT_GRAY);
		b.setOpaque(true);
		balance = new JLabel("<html>$ " + player.getBalance() + "</html>");
		balance.setFont(labelFont);
		balance.setHorizontalAlignment(JLabel.CENTER);
		balance.setBackground(Color.WHITE);
		balance.setOpaque(true);
		p9.add(b);
		p9.add(balance);
		p9.setBorder(borderR);

		JPanel p10 = new JPanel();
		p10.setLayout(new GridLayout(1, 2));
		property = new JLabel("<html>Properties</html>");
		property.setFont(titleFont);
		property.setHorizontalAlignment(JLabel.CENTER);
		property.setBackground(Color.LIGHT_GRAY);
		property.setOpaque(true);
		colorGroups = new JComboBox<>();
		colorGroups.setSelectedItem(null);
		colorGroups.setBackground(Color.LIGHT_GRAY);
		colorGroups.setEnabled(false);
		p10.add(property);
		p10.add(colorGroups);
		p10.setBorder(borderR);

		JPanel p11 = new JPanel();
		JPanel p11L = new JPanel();
		JPanel p11R = new JPanel();
		p11.setLayout(new GridLayout(1, 2));
		p11L.setLayout(new GridLayout(4, 1));
		p11R.setLayout(new GridLayout(4, 1));
		for (int i = 0; i < 4; i++) {
			JLabel detail = new JLabel("");
			detail.setFont(textFont);
			detail.setHorizontalAlignment(JLabel.CENTER);
			detail.setBackground(Color.WHITE);
			detail.setOpaque(true);
			detail.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, false));
			propertyDetails.add(detail);
			JButton sell = new JButton("");
			sell.setFont(buttonFont);
			sell.setBackground(Color.WHITE);
			sell.setEnabled(false);
			sellProperty.add(sell);
			setupSellButton(i);
		}
		for (int i = 0; i < 4; i++) {
			p11L.add(propertyDetails.get(i));
			p11R.add(sellProperty.get(i));
		}
		p11.add(p11L);
		p11.add(p11R);
		p11.setBorder(borderR);

		JPanel p12 = new JPanel();
		p12.setLayout(new GridLayout(1, 2));
		freeCard = new JLabel("<html>Free Card</html>");
		freeCard.setFont(titleFont);
		freeCard.setHorizontalAlignment(JLabel.CENTER);
		freeCard.setBackground(Color.LIGHT_GRAY);
		freeCard.setOpaque(true);
		JPanel p12r = new JPanel();
		p12r.setLayout(new GridLayout(1, 2));
		numOfFreeCard = new JLabel("<html>" + 0 + "</html>");
		numOfFreeCard.setFont(labelFont);
		numOfFreeCard.setHorizontalAlignment(JLabel.CENTER);
		numOfFreeCard.setBackground(Color.WHITE);
		numOfFreeCard.setOpaque(true);
		freeCardLock = new JButton();
		freeCardLock.setBackground(Color.WHITE);
		ImageIcon unlockIcon = new ImageIcon(
				ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "unlock.png"));
		unlockIcon.setImage(unlockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
		freeCardLock.setIcon(unlockIcon);
		freeCardLock.setEnabled(false);
		p12r.add(numOfFreeCard);
		p12r.add(freeCardLock);
		p12.add(freeCard);
		p12.add(p12r);
		p12.setBorder(borderR);

		JPanel p13 = new JPanel();
		p13.setLayout(new GridLayout(1, 2));
		dice1 = new JLabel();
		dice1.setHorizontalAlignment(JLabel.CENTER);
		dice1.setBackground(Color.WHITE);
		dice1.setOpaque(true);
		dice2 = new JLabel();
		dice2.setHorizontalAlignment(JLabel.CENTER);
		dice2.setBackground(Color.WHITE);
		dice2.setOpaque(true);
		p13.add(dice1);
		p13.add(dice2);
		p13.setBorder(borderR);

		playerAction = new JButton("<html>Roll!</html>");
		playerAction.setFont(buttonFont);
		playerAction.setBackground(Color.WHITE);

		playerPanel.add(p8);
		playerPanel.add(p9);
		playerPanel.add(p10);
		playerPanel.add(p11);
		playerPanel.add(p12);
		playerPanel.add(p13);
		playerPanel.add(playerAction);
		playerPanel.setBorder(borderC);

		colorGroups.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getActionCommand().equals("comboBoxChanged")) {
					currentGroup = (ColorGroup) colorGroups.getSelectedItem();
					changeColorGroup();
				}
			}

		});
		freeCardLock.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				changeLockState();
			}

		});
		playerAction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				updatePlayerPanel(true);
				updateBlockPanel(false);
			}

		});

		controlPanel.add(propertyPanel);
		controlPanel.add(playerPanel);
		mainPanel.add(mapPanel);
		mainPanel.add(controlPanel);
		mainPanel.setPreferredSize(new Dimension(WIDTH, HEIGHT));

		JFrame frame = new JFrame("Monopoly");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setContentPane(mainPanel);
		frame.pack();
	}

	/**
	 * Update the info in the block panel when button ROLL is/is not pressed.
	 * 
	 * @param buttonPressed true if button ROLL is pressed
	 */
	private void updateBlockPanel(boolean buttonPressed) {
		if (control.isGameOver()) {
			gameOver();
			blockAction1.setEnabled(false);
			blockAction2.setEnabled(false);
			return;
		}
		GamePlayer player = control.getPlayers().get(currentPlayerId);
		Block block = control.getBlocks().get(player.getPosition());
		if (buttonPressed) {
			if (block instanceof Property)
				control.onProperty((Property) block, player);
			else {
				if (player.isImprisoned() && control.getRollResult() <= 0)
					control.actionInJail(player, control.getOutOfJail(player));
				else if (block instanceof CardPoolBlock && !player.hasDrawnCard())
					control.drawCard(player, player.getPosition());
			}
			endTurn = control.endTurn(block, player);
		}
		System.out.println(player.getName() + "\t@\t" + block.getName() + "\tendturn:\t" + endTurn + "\n");

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				blockAction1.setEnabled(true);
				if (player.isImprisoned()) {
					color.setBackground(Color.WHITE);
					ImageIcon icon = new ImageIcon(
							ClassLoader.getSystemResource("images/infoPanel/blockInfo/" + "jail.png"));
					icon.setImage(icon.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
					color.setIcon(icon);
					blockName.setText("<html>In Jail</html>");
					status.setText("<html>Fine</html>");
					blockStatus.setText("Not Paid");
					blockStatus.setFont(labelFont);
					details.setText("");
					constructionDetails.setText("");
					p.setText("");
					price.setText("");
					blockAction2.setText("");
					int[] lastTwoRolls = player.getLastTwoRolls();
					if ((control.getRollResult() > 0 && lastTwoRolls[0] < 0) || player.hasPaidFine()) {
						blockAction1.setText("");
						blockAction1.setEnabled(false);
						blockAction2.setEnabled(false);
						if (player.hasPaidFine())
							blockStatus.setText("<html>Paid</html>");
					} else {
						switch (control.getOutOfJail(player)) {
						case -1:
							blockAction1.setText("<html>Use Free Card</html>");
							blockAction2.setEnabled(true);
							blockAction2.setText("<html>Pay $ 50 Fine</html>");
							break;
						case -2:
							if (!player.hasPaidFine() || ((lastTwoRolls[0] % 2 != 0 && lastTwoRolls[1] % 2 != 0)
									&& lastTwoRolls[0] > 0 && lastTwoRolls[1] > 0))
								blockAction1.setText("<html>Pay $ 50 Fine</html>");
							blockAction2.setEnabled(false);
							break;
						default:
							blockAction1.setText("<html>Buy Free Card</html>");
							blockAction2.setEnabled(true);
							blockAction2.setText("<html>Pay $ 50 Fine</html>");
							break;
						}
					}
				} else {
					Block b = control.getBlocks().get(player.getPosition());
					color.setBackground(Color.WHITE);
					if (!(b instanceof LandProperty)) {
						ImageIcon icon = new ImageIcon(ClassLoader.getSystemResource("images/infoPanel/blockInfo/"
								+ GameBoard.getBlockIconName(player.getPosition()) + ".png"));
						icon.setImage(icon.getImage().getScaledInstance(60, 60, Image.SCALE_DEFAULT));
						color.setIcon(icon);
					} else
						color.setIcon(null);
					blockName.setText("<html>" + b.getName() + "</html>");
					if (b instanceof Property) {
						if (b instanceof LandProperty) {
							details.setText("<html>Constructions</html>");
							color.setBackground(((LandProperty) b).getColorGroup().getColor());
						} else {
							details.setText("");
						}
						status.setText("<html>Owner</html>");
						blockStatus.setFont(labelFont);
						if (((Property) b).isOwned()) {
							blockAction2.setEnabled(false);
							blockStatus.setText("<html>" + ((Property) b).getOwner().getName() + "</html>");
							p.setText("<html>Rent</html>");
							price.setText("<html>$ " + ((Property) b).getRent() + "</html>");
							if (((Property) b).getOwner() == player) {
								if (b instanceof LandProperty && player.getProperties()
										.containsAll(control.getLandsInGroup(((LandProperty) b).getColorGroup()))) {
									blockAction1.setText("<html>Build Houses</html>");
									blockAction2.setText("");
								} else {
									blockAction1.setText("");
									blockAction1.setEnabled(false);
									blockAction2.setText("");
								}
							} else {
								if (!player.hasPaidRent()) {
									blockAction1.setText("<html>Pay Rent</html>");
									blockAction2.setText("");
								} else {
									blockAction1.setText("");
									blockAction1.setEnabled(false);
									blockAction2.setText("");
								}
							}
						} else {
							blockAction2.setEnabled(true);
							blockStatus.setText("<html>Not owned</html>");
							p.setText("<html>Price</html>");
							price.setText("<html>$ " + ((Property) b).getMortgage() * 2 + "</html>");
							blockAction1.setText("<html>Buy Property</html>");
							blockAction2.setText("<html>Go To Bid</html>");
						}
						if (b instanceof LandProperty && ((LandProperty) b).getBuildings() != null) {
							int numOfHouses = 0, numOfHotels = 0;
							for (BuildingType h : ((LandProperty) b).getBuildings()) {
								if (h == BuildingType.HOTEL)
									numOfHotels++;
								else
									numOfHouses++;
							}
							if (numOfHotels != 0)
								constructionDetails.setText("<html>" + numOfHotels + " Hotel</html>");
							else
								constructionDetails.setText("<html>" + numOfHouses
										+ ((numOfHouses > 1) ? " Houses</html>" : " House</html>"));
						} else
							constructionDetails.setText("");
					} else {
						blockAction2.setEnabled(false);
						if (b instanceof CardPoolBlock) {
							status.setText("<html>Description</html>");
							if (!buttonPressed && !player.hasDrawnCard()) {
								blockStatus.setText("");
								blockAction1.setText("<html>Draw Card</html>");
							} else {
								if (((CardPoolBlock) b).getType() == CardPoolBlock.CardType.CHANCE) {
									blockStatus.setText(Chance.descriptor(player.getCardId()));
								} else {
									blockStatus.setText(CommunityChest.descriptor(player.getCardId()));
								}
								blockStatus.setFont(textFont);
								blockAction1.setText("");
								blockAction1.setEnabled(false);
							}
						} else {
							status.setText("");
							blockStatus.setText("");
							blockAction1.setText("");
							blockAction1.setEnabled(false);
						}
						details.setText("");
						constructionDetails.setText("");
						p.setText("");
						price.setText("");
						blockAction2.setText("");
					}
				}
			}

		});
		thread.start();
	}

	/**
	 * Update the info in the player panel when button ROLL is/is not pressed.
	 * 
	 * @param buttonPressed true if button ROLL is pressed
	 */
	private void updatePlayerPanel(boolean buttonPressed) {
		if (control.isGameOver()) {
			gameOver();
			colorGroups.setEnabled(false);
			for (JButton sell : sellProperty)
				sell.setEnabled(false);
			freeCardLock.setEnabled(false);
			playerAction.setEnabled(false);
			return;
		}
		if (buttonPressed && endTurn) {
			currentPlayerId = (currentPlayerId + 1) % numOfPlayers;
			buttonPressed = false;
			endTurn = false;
			control.newTurn();
		}

		while (control.isRetired(currentPlayerId))
			currentPlayerId = (currentPlayerId + 1) % numOfPlayers;

		GamePlayer player = control.getPlayers().get(currentPlayerId);

		if (buttonPressed && !endTurn && control.getRollResult() == 0) {
			int steps = control.roll(player);
			if (!player.isImprisoned() && steps != -1)
				player.move(steps);
			control.actionsOnBlocks(player, player.getPosition());
		}

		int lastPos = player.getLastPosition();
		control.setCurrentPos(player.getPosition());
		int bankStatus = control.checkPlayerBankStatus(player.getPosition(), player);

		if (buttonPressed)
			endTurn = control.endTurn(control.getBlocks().get(player.getPosition()), player);

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				icon.setBackground(player.getColor());
				playerName.setText("<html>" + player.getName() + "</html>");
				balance.setText("<html>$ " + player.getBalance() + "</html>");
				if (bankStatus <= 0) {
					ImageIcon bankrupt = new ImageIcon(
							ClassLoader.getSystemResource("images/infoPanel/playerInfo/icon/" + "bankrupt.png"));
					bankrupt.setImage(bankrupt.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
					balance.setIcon(bankrupt);
				} else
					balance.setIcon(null);
				int i = 0;
				colorGroups.setSelectedItem(null);
				colorGroups.removeAllItems();
				colorGroups.setRenderer(new MyCellRenderer());
				if (!player.getProperties().isEmpty()) {
					colorGroups.setEnabled(true);
					if (!player.getColorGroups().isEmpty()) {
						for (ColorGroup cg : player.getColorGroups()) {
							colorGroups.insertItemAt(cg, i);
							i++;
						}
						colorGroups.setMaximumRowCount(player.getColorGroups().size());
						colorGroups.setRenderer(new MyCellRenderer());
					}
					ArrayList<Property> ps = player.getPropertiesInGroup(currentGroup);
					for (int j = 0; j < ps.size(); j++)
						sellProperty.get(j).setEnabled(true);
				} else {
					colorGroups.setEnabled(false);
					for (JLabel detail : propertyDetails)
						detail.setText("");
					for (JButton sell : sellProperty) {
						sell.setText("");
						sell.setEnabled(false);
					}
				}
				if (player.hasFreeCard())
					numOfFreeCard.setText("<html>Available</html>");
				else
					numOfFreeCard.setText("<html>None</html>");
				if (player.hasFreeCard()) {
					freeCardLock.setEnabled(true);
					if (!player.isFreeCardLocked()) {
						ImageIcon unlockIcon = new ImageIcon(
								ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "unlock.png"));
						unlockIcon.setImage(unlockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
						freeCardLock.setIcon(unlockIcon);
					} else {
						ImageIcon lockIcon = new ImageIcon(
								ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "lock.png"));
						lockIcon.setImage(lockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
						freeCardLock.setIcon(lockIcon);
					}
				} else {
					freeCardLock.setEnabled(false);
					ImageIcon unlockIcon = new ImageIcon(
							ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "unlock.png"));
					unlockIcon.setImage(unlockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
					freeCardLock.setIcon(unlockIcon);
				}
				ImageIcon icon1 = new ImageIcon(ClassLoader
						.getSystemResource("images/infoPanel/playerInfo/dice/" + control.getResults()[0] + ".png"));
				ImageIcon icon2 = new ImageIcon(ClassLoader
						.getSystemResource("images/infoPanel/playerInfo/dice/" + control.getResults()[1] + ".png"));
				icon1.setImage(icon1.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT));
				icon2.setImage(icon2.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT));
				dice1.setIcon(icon1);
				dice2.setIcon(icon2);
				if (endTurn || control.getRollResult() < 0)
					playerAction.setText("<html>End Turn</html>");
				else
					playerAction.setText("<html>Roll!</html>");
				ArrayList<GamePlayer> playersOnLastBlock = control.getPlayersOnBlock(lastPos);
				ArrayList<GamePlayer> playersOnNewBlock = control.getPlayersOnBlock(player.getPosition());
				ArrayList<Color> colorsLastBlock = new ArrayList<>();
				ArrayList<Color> colorsNewBlock = new ArrayList<>();
				for (GamePlayer p : playersOnLastBlock)
					colorsLastBlock.add(p.getColor());
				for (GamePlayer p : playersOnNewBlock)
					colorsNewBlock.add(p.getColor());
				blocks.get(lastPos).setBorder(compoundBorder(colorsLastBlock));
				blocks.get(player.getPosition()).setBorder(compoundBorder(colorsNewBlock));
			}

		});
		thread.start();
	}

	/**
	 * Update the properties displayed in the player panel.
	 * <p>
	 * Only the properties in the selected color group will be displayed.
	 */
	private void changeColorGroup() {
		GamePlayer player = control.getPlayers().get(currentPlayerId);
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				int i = 0;
				ArrayList<Property> ps = player.getPropertiesInGroup(currentGroup);
				for (Property property : ps) {
					String details = "";
					if (property instanceof LandProperty) {
						details += property.getName();
						int numOfHouses = 0, numOfHotels = 0;
						if (!((LandProperty) property).getBuildings().isEmpty()) {
							for (BuildingType h : ((LandProperty) property).getBuildings()) {
								if (h == BuildingType.HOTEL)
									numOfHotels++;
								else
									numOfHouses++;
							}
						}
						if (numOfHotels != 0)
							details += (": " + numOfHotels + " Hotels");
						else if (numOfHouses != 0)
							details += (": " + numOfHouses + " Houses");
						else
							details += "<br>";
					} else if (!(property instanceof LandProperty)) {
						details += (property.getName());
					}
					propertyDetails.get(i).setText("<html>" + details + "</html>");
					sellProperty.get(i).setText("<html>Sell</html>");
					sellProperty.get(i).setEnabled(true);
					i++;
				}
				if (i <= 3) {
					while (i < 4) {
						propertyDetails.get(i).setText("");
						sellProperty.get(i).setText("");
						sellProperty.get(i).setEnabled(false);
						i++;
					}
				}
			}

		});
		thread.start();
	}

	/**
	 * Sell buttons setup.
	 * 
	 * @param index button index
	 */
	private void setupSellButton(int index) {
		sellProperty.get(index).addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				GamePlayer player = control.getPlayers().get(currentPlayerId);
				ArrayList<Property> properties = player.getPropertiesInGroup(currentGroup);
				control.sellProperty(properties.get(index), player);
				updateBlockPanel(false);
				updatePlayerPanel(false);
			}

		});
	}

	/**
	 * Change the appearance of the lock button when it is pressed.
	 * <p>
	 * Change button L1 & L2 when the player is in jail.
	 */
	private void changeLockState() {
		GamePlayer player = control.getPlayers().get(currentPlayerId);
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				if (player.hasFreeCard()) {
					if (!player.isFreeCardLocked()) {
						ImageIcon lockIcon = new ImageIcon(
								ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "lock.png"));
						lockIcon.setImage(lockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
						freeCardLock.setIcon(lockIcon);
					} else {
						ImageIcon unlockIcon = new ImageIcon(
								ClassLoader.getSystemResource("images/infoPanel/playerInfo/lock/" + "unlock.png"));
						unlockIcon.setImage(unlockIcon.getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
						freeCardLock.setIcon(unlockIcon);
					}
					player.freeCardLock();
					if (player.isImprisoned()) {
						if (!player.isFreeCardLocked()) {
							blockAction1.setText("<html>Use Free Card</html>");
							blockAction2.setText("<html>Pay $ 50 Fine</html>");
							blockAction2.setEnabled(true);
						} else {
							blockAction1.setText("<html>Pay $ 50 Fine</html>");
							blockAction2.setText("");
							blockAction2.setEnabled(false);
						}
					}
				}
			}

		});
		thread.start();
	}

	private void gameOver() {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				logo.setText("<html>GAME OVER</html>");
			}

		});
		thread.start();
	}

	/**
	 * Create compound border with the input colors.
	 * 
	 * @param colors
	 * @return compound border
	 */
	public static Border compoundBorder(ArrayList<Color> colors) {
		if (!colors.isEmpty()) {
			Border border = null;
			if (colors.size() == 1)
				border = BorderFactory.createLineBorder(colors.get(0), 3, false);
			else {
				for (Color color : colors)
					border = BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(color, 3, false),
							border);
			}
			return border;
		} else
			return null;
	}

}
