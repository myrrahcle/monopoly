package gameUI;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import gameControl.GameBoard.ColorGroup;

/**
 * ComboBox renderer
 * 
 * @author Rui
 *
 */
@SuppressWarnings("serial")
public class MyCellRenderer extends JButton implements ListCellRenderer<Object> {
	private boolean b;

	public MyCellRenderer() {
		setOpaque(true);
		b = false;
	}

	@Override
	public void setBackground(Color bg) {
		if (!b)
			return;
		super.setBackground(bg);
	}

	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected,
			boolean hasFocus) {
		b = true;
		setText("");
		if (value != null) {
			if (value instanceof ColorGroup) {
				if (value != ColorGroup.BLACK && value != ColorGroup.WHITE) {
					setBackground(((ColorGroup) value).getColor());
					ImageIcon land = new ImageIcon("./images/infoPanel/playerInfo/properties/" + "lands.png");
					land.setImage(land.getImage().getScaledInstance(160, 40, Image.SCALE_DEFAULT));
					setIcon(land);
				} else if ((ColorGroup) value == ColorGroup.BLACK) {
					setBackground(Color.WHITE);
					ImageIcon rail = new ImageIcon("./images/infoPanel/playerInfo/properties/" + "railway.png");
					rail.setImage(rail.getImage().getScaledInstance(160, 40, Image.SCALE_DEFAULT));
					setIcon(rail);
				} else if ((ColorGroup) value == ColorGroup.WHITE) {
					setBackground(Color.WHITE);
					ImageIcon util = new ImageIcon("./images/infoPanel/playerInfo/properties/" + "utilities.png");
					util.setImage(util.getImage().getScaledInstance(160, 40, Image.SCALE_DEFAULT));
					setIcon(util);
				}
			}
		}
		b = false;
		return this;
	}

}
