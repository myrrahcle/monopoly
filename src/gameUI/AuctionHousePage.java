package gameUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

import blocks.Property;
import gameControl.GameControl;
import players.GamePlayer;

/**
 * Auction page
 * 
 * @author Rui
 *
 */
@SuppressWarnings("serial")
public class AuctionHousePage extends JFrame {
	private static final int WIDTH = 1850;
	private static final int HEIGHT = 925;
	private Font titleFont, labelFont, buttonFont;
	private Border borderC, borderR;
	private ArrayList<JLabel> bids, topPlayersIcons, topPlayersNames, topPlayersBids;
	private JButton price;
	private int currentBid, numOfBidders;
	private Property currentItem;
	private ArrayList<Integer> playerBids;
	private ArrayList<GamePlayer> players, topBids, notOnList;

	private GameControl control;

	public AuctionHousePage() {
		this.control = GameBoardPage.control;
		for (Property p : control.getProperties()) {
			if (p.getPosition() == control.getCurrentPos()) {
				this.currentItem = p;
				break;
			}
		}
		this.currentBid = currentItem.getMortgage();
		this.playerBids = new ArrayList<>();
		this.topBids = new ArrayList<>();
		this.notOnList = new ArrayList<>();
		this.players = control.getPlayers();
		this.numOfBidders = players.size();
		for (GamePlayer player : players)
			playerBids.add(player.getId(), currentBid);
		for (int i = 0; i < Math.min(players.size(), 3); i++)
			topBids.add(i, players.get(i));
		for (int i = 0; i < players.size() - topBids.size(); i++)
			notOnList.add(i, players.get(i + topBids.size()));
		this.bids = new ArrayList<>();
		this.topPlayersIcons = new ArrayList<>();
		this.topPlayersNames = new ArrayList<>();
		this.topPlayersBids = new ArrayList<>();
		setUpPage();
	}

	/**
	 * Page setup.
	 */
	private void setUpPage() {
		titleFont = new Font("title", Font.BOLD, 20);
		labelFont = new Font("label", Font.ROMAN_BASELINE, 18);
		buttonFont = new Font("button", Font.BOLD, 20);

		borderC = BorderFactory.createLineBorder(Color.BLACK, 2, false);
		borderR = BorderFactory.createEmptyBorder(2, 2, 2, 2);

		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();

		ArrayList<JPanel> playersPanel = new ArrayList<>();
		for (int i = 0; i < players.size(); i++) {
			JPanel panel = setupPlayerInfo(i);

			JLabel label = new JLabel("<html>$ " + playerBids.get(i) + "</html>");
			label.setFont(labelFont);
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setBackground(Color.WHITE);
			label.setOpaque(true);
			panel.add(label, 3);
			bids.add(i, label);
			playersPanel.add(i, panel);
		}

		p4.setLayout(new GridLayout(10, 1));
		JPanel p4u = new JPanel();
		JLabel balance = new JLabel("<html>Balance</html>");
		balance.setFont(titleFont);
		balance.setHorizontalAlignment(JLabel.CENTER);
		balance.setBackground(Color.WHITE);
		balance.setOpaque(true);
		JLabel order = new JLabel("<html>Bid</html>");
		order.setFont(titleFont);
		order.setHorizontalAlignment(JLabel.CENTER);
		order.setBackground(Color.WHITE);
		order.setOpaque(true);
		p4u.setLayout(new GridLayout(1, 6));
		p4u.add(new JPanel());
		p4u.add(new JPanel());
		p4u.add(balance);
		p4u.add(order);
		p4u.add(new JPanel());
		p4u.add(new JPanel());
		p4.add(p4u);
		for (JPanel panel : playersPanel) {
			p4.add(panel);
		}
		p4.setBorder(borderC);

		JLabel item = new JLabel();
		ImageIcon icon = new ImageIcon(
				ClassLoader.getSystemResource("images/deeds/" + currentItem.getPosition() + ".png"));
		icon.setImage(icon.getImage().getScaledInstance(192, 316, Image.SCALE_DEFAULT));
		item.setIcon(icon);
		item.setHorizontalAlignment(JLabel.CENTER);
		item.setBackground(Color.WHITE);
		item.setOpaque(true);
		item.setBorder(borderC);

		JLabel l1 = new JLabel("<html>Current Bid</html>");
		l1.setFont(titleFont);
		l1.setHorizontalAlignment(JLabel.CENTER);
		l1.setBackground(Color.WHITE);
		l1.setOpaque(true);

		price = new JButton("<html>$ " + currentBid + "</html>");
		price.setFont(titleFont);
		price.setHorizontalAlignment(JLabel.CENTER);
		price.setBackground(Color.WHITE);
		price.setOpaque(true);

		price.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (isSold() >= -1) {
					control.setBidResult(isSold(), currentBid);
					dispose();
				}
			}

		});

		JPanel p3u = new JPanel();
		p3u.setLayout(new GridLayout(1, 2));
		p3u.add(l1);
		p3u.add(price);

		ArrayList<JPanel> topBidsPanel = new ArrayList<>();
		for (int i = 0; i < Math.min(players.size(), 3); i++) {
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout(1, 3));

			JLabel label1 = new JLabel();
			label1.setHorizontalAlignment(JLabel.CENTER);
			label1.setBackground(topBids.get(i).getColor());
			label1.setOpaque(true);
			JLabel label2 = new JLabel("<html>" + topBids.get(i).getName() + "</html>");
			label2.setFont(titleFont);
			label2.setHorizontalAlignment(JLabel.CENTER);
			label2.setBackground(Color.WHITE);
			label2.setOpaque(true);
			JLabel label3 = new JLabel("<html>$ " + playerBids.get(topBids.get(i).getId()) + "</html>");
			label3.setFont(labelFont);
			label3.setHorizontalAlignment(JLabel.CENTER);
			label3.setBackground(Color.WHITE);
			label3.setOpaque(true);

			panel.add(label1);
			panel.add(label2);
			panel.add(label3);
			panel.setBorder(borderR);

			topPlayersIcons.add(i, label1);
			topPlayersNames.add(i, label2);
			topPlayersBids.add(i, label3);
			topBidsPanel.add(i, panel);
		}

		p3.setLayout(new GridLayout(4, 1));
		p3.add(p3u);
		for (JPanel panel : topBidsPanel) {
			p3.add(panel);
		}

		p2.setLayout(new GridLayout(2, 1));
		p2.add(item);
		p2.add(p3);
		p2.setBorder(borderC);

		p1.setLayout(new GridLayout(1, 2));
		p1.add(p2);
		p1.add(p4);
		p1.setPreferredSize(new Dimension(WIDTH, HEIGHT));

		this.setName("Auction House");
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(p1);
		this.pack();
	}

	/**
	 * Determine whether the property has been sold.
	 * 
	 * @return id of the buyer
	 */
	private int isSold() {
		if (numOfBidders == 1) {
			int playerBid = playerBids.get(topBids.get(0).getId());
			if (currentBid == playerBid)
				return topBids.get(0).getId();
			else
				return -1;
		} else
			return -2;
	}

	/**
	 * Player info panel setup.
	 * 
	 * @param id id of the player
	 * @return one row in the panel
	 */
	private JPanel setupPlayerInfo(int id) {
		GamePlayer player = control.getPlayers().get(id);
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 6));

		JLabel avatar = new JLabel();
		JLabel name = new JLabel();
		JLabel balance = new JLabel();
		final JButton bid = new JButton("<html>Bid!</html>");
		final JButton leave = new JButton("<html>Leave</html>");

		avatar = new JLabel();
		avatar.setHorizontalAlignment(JLabel.CENTER);
		avatar.setBackground(player.getColor());
		avatar.setOpaque(true);
		name = new JLabel("<html>" + player.getName() + "</html>");
		name.setFont(titleFont);
		name.setHorizontalAlignment(JLabel.CENTER);
		name.setBackground(Color.WHITE);
		name.setOpaque(true);
		balance = new JLabel("<html>$ " + player.getBalance() + "</html>");
		balance.setFont(labelFont);
		balance.setHorizontalAlignment(JLabel.CENTER);
		balance.setBackground(Color.WHITE);
		balance.setOpaque(true);

		bid.setFont(buttonFont);
		bid.setBackground(Color.WHITE);
		leave.setFont(buttonFont);
		leave.setBackground(Color.WHITE);

		bid.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (isSold() == -2) {
					int b = currentBid;
					b += 10;
					currentBid = b;
					playerBids.set(id, b);
					if (topBids.contains(player)) {
						int index = topBids.indexOf(player);
						if (index != 0) {
							topBids.remove(player);
							topBids.add(0, player);
						}
					} else {
						GamePlayer removedPlayer = topBids.remove(2);
						notOnList.remove(removedPlayer);
						notOnList.add(0, removedPlayer);
						topBids.add(0, player);
					}
					updatePlayersPanel(id);
					updateTopBidsPanel();
				}
			}

		});
		leave.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (isSold() == -2) {
					numOfBidders--;
					bid.setEnabled(false);
					leave.setEnabled(false);
					notOnList.remove(player);
					if (topBids.contains(player)) {
						topBids.remove(player);
						updateTopBidsPanel();
					}
				}
				if (isSold() >= 0) {
					ImageIcon icon = new ImageIcon(
							ClassLoader.getSystemResource("images/auctionHouse/" + "deal.png"));
					icon.setImage(icon.getImage().getScaledInstance(80, 80, Image.SCALE_DEFAULT));
					price.setIcon(icon);
				} else if (isSold() == -1) {
					ImageIcon icon = new ImageIcon(
							ClassLoader.getSystemResource("images/auctionHouse/" + "unsold.png"));
					icon.setImage(icon.getImage().getScaledInstance(80, 55, Image.SCALE_DEFAULT));
					price.setIcon(icon);
				}
			}

		});

		panel.add(avatar);
		panel.add(name);
		panel.add(balance);
		panel.add(bid);
		panel.add(leave);
		panel.setBorder(borderR);
		return panel;
	}

	/**
	 * Update the top bids panel.
	 */
	private void updateTopBidsPanel() {
		if (topBids.size() < 3 && numOfBidders >= 3)
			topBids.add(2, notOnList.remove(0));

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < ((players.size() > 3) ? 3 : players.size()); i++) {
					if (i < topBids.size()) {
						topPlayersIcons.get(i).setBackground(topBids.get(i).getColor());
						topPlayersNames.get(i).setText(topBids.get(i).getName());
						topPlayersBids.get(i).setText("<html>$ " + playerBids.get(topBids.get(i).getId()) + "</html>");
					} else {
						if (numOfBidders < 3) {
							topPlayersIcons.get(i).setBackground(Color.WHITE);
							topPlayersNames.get(i).setText("");
							topPlayersBids.get(i).setText("");
						}
					}
				}
				price.setText("<html>$ " + currentBid + "</html>");
			}

		});
		thread.start();
	}

	/**
	 * Update the players info panel.
	 * 
	 * @param id player id
	 */
	private void updatePlayersPanel(int id) {
		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				bids.get(id).setText("<html>$ " + playerBids.get(id) + "</html>");
			}

		});
		thread.start();
	}

}
