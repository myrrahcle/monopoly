package cards;

/**
 * Descriptions of Title Deed cards
 * 
 * @author Mudi
 *
 */
public class TitleDeedCard {

	private static Object[] info;

	/**
	 * Get the description of a Title Deed card.
	 * 
	 * @param position position of the property
	 * @return an array containing the name of the property and:
	 *         <p>
	 *         if constructible: base rent; rent with 1, 2, 3, 4 houses; a hotel;
	 *         price for building a house; mortgage value
	 *         <p>
	 *         if is railroad: base rent; rent if 2, 3, 4 of the railroads are
	 *         owned; mortgage value
	 *         <p>
	 *         if is water works/electronic company: mortgage value
	 */
	public static Object[] descriptor(int position) {

		switch (position) {
		case 1:
			info = new Object[] { "Mediterranean Avenue", 2, 10, 30, 90, 160, 250, 50, 30 };
			break;
		case 3:
			info = new Object[] { "Baltic Avenue", 4, 20, 60, 180, 320, 450, 50, 30 };
			break;
		case 5:
			info = new Object[] { "Reading Railroad", 25, 50, 100, 200, 100 };
			break;
		case 6:
			info = new Object[] { "Oriental Avenue", 6, 30, 90, 270, 400, 550, 50, 50 };
			break;
		case 8:
			info = new Object[] { "Vermont Avenue", 6, 30, 90, 270, 400, 550, 50, 50 };
			break;
		case 9:
			info = new Object[] { "Connecticut Avenue", 8, 40, 100, 300, 450, 600, 50, 60 };
			break;
		case 11:
			info = new Object[] { "St. Charles Place", 10, 50, 150, 450, 625, 750, 100, 70 };
			break;
		case 12:
			info = new Object[] { "Electric Company", 75 };
			break;
		case 13:
			info = new Object[] { "States Avenue", 10, 50, 150, 450, 625, 750, 100, 70 };
			break;
		case 14:
			info = new Object[] { "Virginia Avenue", 12, 60, 180, 500, 700, 900, 100, 80 };
			break;
		case 15:
			info = new Object[] { "Pennsylvania Railroad", 25, 50, 100, 200, 100 };
			break;
		case 16:
			info = new Object[] { "St. James Place", 14, 70, 200, 550, 750, 950, 100, 90 };
			break;
		case 18:
			info = new Object[] { "Tennessee Avenue", 14, 70, 200, 550, 750, 950, 100, 90 };
			break;
		case 19:
			info = new Object[] { "New York Avenue", 16, 80, 220, 600, 800, 1000, 100, 100 };
			break;
		case 21:
			info = new Object[] { "Kentucky Avenue", 18, 90, 250, 700, 875, 1050, 150, 110 };
			break;
		case 23:
			info = new Object[] { "Indiana Avenue", 18, 90, 250, 700, 875, 1050, 150, 110 };
			break;
		case 24:
			info = new Object[] { "Illinois Avenue", 20, 100, 300, 750, 925, 1100, 150, 120 };
			break;
		case 25:
			info = new Object[] { "B & O Railroad", 25, 50, 100, 200, 100 };
			break;
		case 26:
			info = new Object[] { "Atlantic Avenue", 22, 110, 330, 800, 975, 1150, 150, 130 };
			break;
		case 27:
			info = new Object[] { "Ventnor Avenue", 22, 110, 300, 800, 975, 1150, 150, 130 };
			break;
		case 28:
			info = new Object[] { "Water Works", 75 };
			break;
		case 29:
			info = new Object[] { "Marvin Gardens", 24, 120, 360, 850, 1025, 1200, 150, 140 };
			break;
		case 31:
			info = new Object[] { "Pacific Avenue", 26, 130, 390, 900, 1100, 1275, 200, 150 };
			break;
		case 32:
			info = new Object[] { "North Carolina Avenue", 26, 130, 390, 900, 1100, 1275, 200, 150 };
			break;
		case 34:
			info = new Object[] { "Pennsylvania Avenue", 28, 150, 450, 1000, 1200, 1400, 200, 160 };
			break;
		case 35:
			info = new Object[] { "Short Line Railroad", 25, 50, 100, 200, 100 };
			break;
		case 37:
			info = new Object[] { "Park Place", 35, 175, 500, 1100, 1300, 1500, 200, 175 };
			break;
		case 39:
			info = new Object[] { "Broadwalk", 50, 200, 600, 1400, 1700, 2000, 200, 200 };
			break;
		}
		return info;
	}

}
