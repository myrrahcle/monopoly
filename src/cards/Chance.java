package cards;

/**
 * Descriptions of Chance cards
 * 
 * @author Mudi
 *
 */
public class Chance {

	/**
	 * Get the description of a Chance card.
	 * 
	 * @param index card id
	 * @return description
	 */
	public static String descriptor(int index) {
		String description = null;

		switch (index) {
		case 0:
			description = "<html><center>Advance to \"Go\".</center></html>";
			break;
		case 1:
			description = "<html><center>Advance to Marvin Gardens (29).<br>If you pass Go, collect $200.</center></html>";
			break;
		case 2:
			description = "<html><center>Advance to St. James Place (16).<br>If you pass Go, collect $200.</center></html>";
			break;
		case 3:
			description = "<html><center>Advance token to nearest Utility.<br>If unowned, you may buy it from the Bank.<br>If owned, throw dice and pay owner<br>a total 10 times the amount thrown.</center></html>";
			break;
		case 4:
			description = "<html><center>Advance token to the nearest Railroad,<br>and pay owner twice the rental<br>to which he/she is otherwise entitled.<br>If Railroad is unowned,<br>you may buy it from the bank.</center></html>";
			break;
		case 5:
			description = "<html><center>Bank pays you dividend of $50.</center></html>";
			break;
		case 6:
			description = "<html><center>Get out of Jail Free.</center></html>";
			break;
		case 7:
			description = "<html><center>Go back three spaces.</center></html>";
			break;
		case 8:
			description = "<html><center>Go directly to Jail.</center></html>";
			break;
		case 9:
			description = "<html><center>Make general repairs on all your property:<br>For each house pay $25, for each hotel $100.</center></html>";
			break;
		case 10:
			description = "<html><center>Pay poor tax of $15.</center></html>";
			break;
		case 11:
			description = "<html><center>Take a trip to Reading Railroad.<br>If you pass Go, collect $200.</center></html>";
			break;
		case 12:
			description = "<html><center>Take a walk on the Free Parking.<br>Advance token to Free Parking directly.</center></html>";
			break;
		case 13:
			description = "<html><center>You have been elected Chairman of the Board.<br>Pay each player $50.</center></html>";
			break;
		case 14:
			description = "<html><center>Bank pays you dividend of $50.</center></html>";
			break;
		case 15:
			description = "<html><center>Your building loan matures.<br>Receive $150.</center></html>";
			break;
		}
		return description;
	}

}
