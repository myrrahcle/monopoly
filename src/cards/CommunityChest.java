package cards;

/**
 * Descriptions of Community Chest cards.
 * 
 * @author Mudi
 *
 */
public class CommunityChest {

	/**
	 * Get the description of a Community Chest card.
	 * 
	 * @param index card id
	 * @return description
	 */
	public static String descriptor(int index) {
		String description = null;

		switch (index) {
		case 0:
			description = "<html><center>Advance to \"Go\".</center></html>";
			break;
		case 1:
			description = "<html><center>Bank error in your favor.<br>Collect $200.</center></html>";
			break;
		case 2:
			description = "<html><center>Doctor's fees.<br>Pay $50.</center></html>";
			break;
		case 3:
			description = "<html><center>From sale of stock you get $50.</center></html>";
			break;
		case 4:
			description = "<html><center>Get out of Jail Free.</center></html>";
			break;
		case 5:
			description = "<html><center>Go directly to Jail.</center></html>";
			break;
		case 6:
			description = "<html><center>Grand Opera Night.<br>Collect $50 from every player<br>for opening night seats.</center></html>";
			break;
		case 7:
			description = "<html><center>Holiday Fund matures.<br>Receive $100.</center></html>";
			break;
		case 8:
			description = "<html><center>Income tax refund.<br>Collect $20.</center></html>";
			break;
		case 9:
			description = "<html><center>Life insurance matures.<br>Collect $100.</center></html>";
			break;
		case 10:
			description = "<html><center>Hospital Fees.<br>Pay $50.</center></html>";
			break;
		case 11:
			description = "<html><center>School fees.<br>Pay $50.</center></html>";
			break;
		case 12:
			description = "<html><center>Receive $25 consultancy fee.</center></html>";
			break;
		case 13:
			description = "<html><center>You are assessed for street repairs:<br>Pay $40 per house and $115 per hotel you own.</center></html>";
			break;
		case 14:
			description = "<html><center>You have won second prize in a beauty contest.<br>Collect $10.</center></html>";
			break;
		case 15:
			description = "<html><center>You inherit $100.</center></html>";
			break;
		}
		return description;

	}

}
