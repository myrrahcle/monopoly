package blocks;

import java.util.ArrayList;

import gameControl.GameBoard.ColorGroup;
import gameControl.GameBoard.BuildingType;

/**
 * Constructible property
 * 
 * @author Rui
 *
 */
public class LandProperty extends Property {
	private ColorGroup colorGroup;
	private int housePrice;
	private ArrayList<BuildingType> buildings;

	public LandProperty() {
		buildings = new ArrayList<>();
	}

	/**
	 * Set the color group of the property.
	 * 
	 * @param color color group
	 */
	public void setColorGroup(ColorGroup color) {
		this.colorGroup = color;
	}

	/**
	 * Get the color group of the property.
	 * 
	 * @return color group
	 */
	public ColorGroup getColorGroup() {
		return colorGroup;
	}

	/**
	 * Set the house price of the property.
	 * 
	 * @param price house price
	 */
	public void setHousePrice(int price) {
		this.housePrice = price;
	}

	/**
	 * Get the house price of the property.
	 * 
	 * @return house price
	 */
	public int getHousePrice() {
		return housePrice;
	}

	/**
	 * Build a house on the property.
	 */
	public void buildHouse() {
		buildings.add(BuildingType.HOUSE);
	}

	/**
	 * Build a hotel on the property.
	 */
	public void buildHotel() {
		buildings.clear();
		buildings.add(BuildingType.HOTEL);
	}

	/**
	 * Sell a building on the property.
	 */
	public void sellHouse() {
		buildings.remove(0);
	}

	/**
	 * Get the current buildings on the property.
	 * 
	 * @return list of buildings
	 */
	public ArrayList<BuildingType> getBuildings() {
		return buildings;
	}

	/**
	 * Determine whether there is a hotel on the property.
	 * 
	 * @return true if there is a hotel
	 */
	public boolean hasHotel() {
		for (BuildingType h : buildings) {
			if (h == BuildingType.HOTEL)
				return true;
		}
		return false;
	}
}
