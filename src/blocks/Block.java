package blocks;

/**
 * Generic block
 * 
 * @author Rui
 *
 */
public class Block {
	private int position;
	private String name;

	/**
	 * Set the position of the block.
	 * 
	 * @param position
	 */
	public void setPosition(int position) {
		this.position = position;
	}

	/**
	 * Get the position of the block.
	 * 
	 * @return position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Set the name of the block
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the name of the block.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
}
