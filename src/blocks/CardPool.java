package blocks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Card stack
 * 
 * @author Rui
 *
 */
public class CardPool {

	private static List<Integer> chances = new ArrayList<>();
	private static List<Integer> communityChests = new ArrayList<>();

	/**
	 * Shuffle the cards in the stack.
	 */
	public static boolean shuffle() {
		for (int i = 0; i < 16; i++) {
			chances.add((Integer) i);
			communityChests.add((Integer) i);
		}
		Collections.shuffle(chances);
		Collections.shuffle(communityChests);
		return true;
	}

	/**
	 * Draw one Chance card from the top of the stack.
	 * <p>
	 * Put it back to the bottom of the stack.
	 * 
	 * @return card id
	 */
	public static int drawChanceCard() {
		chances.add(chances.get(0));
		return chances.remove(0);
	}

	/**
	 * Draw one Community Chest card from the top of the stack.
	 * <p>
	 * Put it back to the bottom of the stack.
	 * 
	 * @return card id
	 */
	public static int drawCCCard() {
		communityChests.add(communityChests.get(0));
		return communityChests.remove(0);
	}

	/**
	 * Remove 'Get Out Of Jail Free' card from stack of Chance cards.
	 */
	public static void removeChanceFreeCard() {
		Iterator<Integer> iterator = chances.iterator();
		while (iterator.hasNext()) {
			Integer id = iterator.next();
			if (id == 6)
				iterator.remove();
		}
	}

	/**
	 * Remove 'Get Out Of Jail Free' card from stack of Community Chest cards.
	 */
	public static void removeCCFreeCard() {
		Iterator<Integer> iterator = communityChests.iterator();
		while (iterator.hasNext()) {
			Integer id = iterator.next();
			if (id == 4)
				iterator.remove();
		}
	}

	/**
	 * Return 'Get Out Of Jail Free' card back to stack of Chance cards.
	 */
	public static void returnChanceFreeCard() {
		chances.add(15, 6);
	}

	/**
	 * Return 'Get Out Of Jail Free' card back to stack of Community Chest cards.
	 */
	public static void returnCCFreeCard() {
		communityChests.add(15, 4);
	}

}
