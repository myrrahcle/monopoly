package blocks;

import players.GamePlayer;

/**
 * Property the can be traded
 * 
 * @author Rui
 *
 */
public class Property extends Block {
	private int mortgage, rent;
	private GamePlayer owner;

	/**
	 * Set the mortgage of the property.
	 * 
	 * @param mortgage
	 */
	public void setMortgage(int mortgage) {
		this.mortgage = mortgage;
	}

	/**
	 * Get the mortgage of the property.
	 * 
	 * @return mortgage
	 */
	public int getMortgage() {
		return mortgage;
	}

	/**
	 * Set the rent of the property.
	 * 
	 * @param rent
	 */
	public void setRent(int rent) {
		this.rent = rent;
	}

	/**
	 * Get the rent of the property.
	 * 
	 * @return rent
	 */
	public int getRent() {
		return rent;
	}

	/**
	 * Set the owner of the property.
	 * 
	 * @param player owner
	 */
	public void setOwner(GamePlayer player) {
		this.owner = player;
	}

	/**
	 * Get the owner of the property.
	 * 
	 * @return owner
	 */
	public GamePlayer getOwner() {
		return owner;
	}

	/**
	 * Determine whether the property is owned.
	 * 
	 * @return true if it is owned
	 */
	public boolean isOwned() {
		return owner != null;
	}
}
