package blocks;

/**
 * Chance/Community Chest block
 * 
 * @author Rui
 *
 */
public class CardPoolBlock extends Block {
	/**
	 * Card type
	 * 
	 * @author Rui
	 *
	 */
	public enum CardType {
		CHANCE, COMMUNITYCHEST
	}

	private String description;
	private CardType type;

	/**
	 * Set the description of a card.
	 * 
	 * @param des description
	 */
	public void setDescription(String des) {
		this.description = des;
	}

	/**
	 * Get the description of the card.
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Set the type of the card.
	 * 
	 * @param type
	 */
	public void setType(CardType type) {
		this.type = type;
	}

	/**
	 * Get the type of the card.
	 * 
	 * @return type
	 */
	public CardType getType() {
		return type;
	}
}
