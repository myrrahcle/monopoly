package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import blocks.LandProperty;

/**
 * Test of LandProperty
 * 
 * @author Mudi
 *
 */

class LandPropertyTest {
	
	LandProperty temp = new LandProperty();

	@Test
	void housePricetest() {
		temp.setHousePrice(180);
		assertEquals("The price should be 180", 180, temp.getHousePrice());
	}
	
	@Test
	void houseTest() {
		assertFalse(temp.hasHotel());
		temp.buildHouse();
		assertFalse(temp.hasHotel());
		temp.buildHotel();
		assertTrue(temp.hasHotel());		
	}

}
