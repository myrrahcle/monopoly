package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import blocks.Property;
import players.GamePlayer;

/**
 * Test of Property
 * 
 * @author Mudi
 *
 */

class PropertyTest {
	
	Property temp = new Property();
	
	@Test
	void ownedTest() {
		assertFalse(temp.isOwned());
	}

	@Test
	void mortgateTest() {
		temp.setMortgage(3);
		assertEquals("The mortgage should be 3", 3, temp.getMortgage());
	}
	
	@Test
	void rentTest() {
		temp.setRent(250);
		assertEquals("The rent should be 250", 250, temp.getRent());
	}
	
	@Test
	void ownerTest() {
		GamePlayer aaa = new GamePlayer();
		temp.setOwner(aaa);
		assertEquals("The owner should be aaa", aaa, temp.getOwner());
	}

}
