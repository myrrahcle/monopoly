package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import gameControl.GameBoard;

/**
 * Test of GameBoard
 * 
 * @author Mudi
 *
 */

class GameBoardTest {
	

	@Test
	void blockTest() {
		GameBoard temp = new GameBoard();
		
		assertEquals("The name of the block should be: go", "go", temp.getBlockIconName(0));
		assertEquals("The name of the block should be: communityChest", "communityChest", temp.getBlockIconName(33));
		assertEquals("The name of the block should be: tax", "tax", temp.getBlockIconName(38));
		assertEquals("The name of the block should be: railroad", "railroad", temp.getBlockIconName(35));
		assertEquals("The name of the block should be: chance", "chance", temp.getBlockIconName(36));
		assertEquals("The name of the block should be: jail", "jail", temp.getBlockIconName(10));
		assertEquals("The name of the block should be: electricCompany", "electricCompany", temp.getBlockIconName(12));
		assertEquals("The name of the block should be: freeParking", "freeParking", temp.getBlockIconName(20));
		assertEquals("The name of the block should be: waterWorks", "waterWorks", temp.getBlockIconName(28));
		assertEquals("The name of the block should be: goToJail", "goToJail", temp.getBlockIconName(30));
		
	}
	

}
