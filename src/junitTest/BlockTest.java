package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import blocks.Block;

/**
 * Test of Block
 * 
 * @author Mudi
 *
 */
class BlockTest {
	
	Block temp = new Block();

	@Test
	void positiontest() {
		temp.setPosition(23);
		assertEquals("The position should be 23", 23, temp.getPosition());
	}
	
	@Test
	void nameTest() {
		temp.setName("aaa");
		assertEquals("The name should be aaa", "aaa", temp.getName());		
	}

}
