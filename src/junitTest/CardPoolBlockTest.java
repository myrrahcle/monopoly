package junitTest;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import blocks.CardPoolBlock;
import blocks.CardPoolBlock.CardType;

/**
 * Test of CardPoolBlock
 * 
 * @author Mudi
 *
 */

class CardPoolBlockTest {
	
	CardPoolBlock temp = new CardPoolBlock();

	@Test
	void descriptionTest() {
		temp.setDescription("aaa");
		assertEquals("The description should be aaa", "aaa", temp.getDescription());
	}
	
	@Test
	void typeTest() {
		temp.setType(CardType.CHANCE);
		assertEquals("The type should be CHANCE", CardType.CHANCE, temp.getType());
	}
	

}
