package junitTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import blocks.CardPool;

/**
 * Test of CardPool
 * 
 * @author Mudi
 *
 */

class CardPoolTest {
	
	CardPool temp = new CardPool();

	@Test
	void shuffleTest() {
		assertTrue(temp.shuffle());
	}
	
	@Test
	void returnCardTest() {
		temp.removeCCFreeCard();
		temp.removeChanceFreeCard();
	}

}
