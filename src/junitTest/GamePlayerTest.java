package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Color;

import org.junit.jupiter.api.Test;

import blocks.Block;
import players.GamePlayer;

/**
 * Test of GamePlayer
 * 
 * @author Mudi
 *
 */

class GamePlayerTest {
	
	GamePlayer temp = new GamePlayer();

	@Test
	void idTest() {		
		temp.setId(20);
		assertEquals("ID should be 20.", 20, temp.getId());
	}
	
	@Test
	void nameTest() {
		temp.setName("aaa");
		assertEquals("Name should be aaa", "aaa", temp.getName());
	}
	
	@Test
	void colorTest() {
		temp.setColor(Color.blue);
		assertEquals("Color should be blue", Color.blue, temp.getColor());		
	}
	
	@Test
	void retiredTest() {
		assertFalse(temp.isRetired());		
	}
	
	@Test
	void lastTwoRollsTest() {
		temp.setLastRoll(2);
		assertEquals("Last two rolls should be 2", 2, temp.getLastTwoRolls()[0]);
	}
	
	@Test
	void prisonTest() {
		assertFalse(temp.isImprisoned());  // The player should not in the prison at first
		temp.changeImprisonState();
		assertTrue(temp.isImprisoned());
	}
	
	@Test
	void freeCardTest() {
		assertFalse(temp.hasFreeCard());
		temp.obtainFreeCard(6);
		assertTrue(temp.hasFreeCard());
		temp.useFreeCard();
		assertFalse(temp.hasFreeCard());
		temp.freeCardLock();
		assertTrue(temp.isFreeCardLocked());
	}
	
	@Test
	void payFineTest() {
		temp.payFine();
		assertTrue(temp.hasPaidFine());
	}
	
	@Test
	void payRentTest() {
		assertFalse(temp.hasPaidRent());
		temp.payRent();
		assertTrue(temp.hasPaidRent());
	}
	
	@Test
	void cardTest() {
		temp.drawCard(13);
		assertEquals("The card should be 13", 13, temp.getCardId());
		assertTrue(temp.hasDrawnCard());
	}
	
	@Test
	void moveTest() {
		temp.moveTo(30);
		assertEquals("The player should move to 30", 30, temp.getPosition());
		temp.move(3);
		assertEquals("The last positiono of the player should be 30", 30, temp.getLastPosition());
		assertEquals("The player should move to 33", 33, temp.getPosition());
	}
	
	@Test
	void incomeTest() {
		int balance = temp.getBalance();
		temp.income(200);
		assertEquals("The player should get 200 extra", balance+200, temp.getBalance());
	}
	
	
}
