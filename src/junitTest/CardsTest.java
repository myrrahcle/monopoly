package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import cards.Chance;
import cards.CommunityChest;
import cards.TitleDeedCard;

/**
 * Test of package cards
 * 
 * @author Mudi
 *
 */

class CardsTest {

	@Test
	void Chancetest() {
		Chance temp = new Chance();
		
		String expected = "<html><center>Bank pays you dividend of $50.</center></html>";
		assertEquals("The description should be: "
				+ "<html><center>Bank pays you dividend of $50.</center></html>", expected, temp.descriptor(14));
		
		expected = "<html><center>Bank pays you dividend of $50.</center></html>";
		assertEquals("The description should be: "
				+ "\"<html><center>Bank pays you dividend of $50.</center></html>\"", expected, temp.descriptor(5));
		
		expected = "<html><center>Pay poor tax of $15.</center></html>";
		assertEquals("The description should be: "
				+ "<html><center>Pay poor tax of $15.</center></html>", expected, temp.descriptor(10));
		
		expected = "<html><center>Advance to \"Go\".</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(0));
		expected = "<html><center>Advance to Marvin Gardens (29).<br>If you pass Go, collect $200.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(1));
		expected = "<html><center>Advance to St. James Place (16).<br>If you pass Go, collect $200.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(2));
		expected = "<html><center>Advance token to nearest Utility.<br>If unowned, you may buy it from the Bank.<br>If owned, throw dice and pay owner<br>a total 10 times the amount thrown.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(3));
		expected = "<html><center>Advance token to the nearest Railroad,<br>and pay owner twice the rental<br>to which he/she is otherwise entitled.<br>If Railroad is unowned,<br>you may buy it from the bank.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(4));
		expected = "<html><center>Get out of Jail Free.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(6));
		expected = "<html><center>Go back three spaces.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(7));
		expected = "<html><center>Go directly to Jail.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(8));
		expected = "<html><center>Make general repairs on all your property:<br>For each house pay $25, for each hotel $100.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(9));
		expected = "<html><center>Take a trip to Reading Railroad.<br>If you pass Go, collect $200.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(11));
		expected = "<html><center>Take a walk on the Free Parking.<br>Advance token to Free Parking directly.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(12));
		expected = "<html><center>You have been elected Chairman of the Board.<br>Pay each player $50.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(13));
		expected = "<html><center>Your building loan matures.<br>Receive $150.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(15));
	}
	
	@Test
	void CommunityChestTest() {
		CommunityChest temp = new CommunityChest();
		
		String expected = "<html><center>Advance to \"Go\".</center></html>";
		assertEquals("The description should be: "
				+ "<html><center>Advance to \\\"Go\\\".</center></html>", expected, temp.descriptor(0));
		
		expected = "<html><center>School fees.<br>Pay $50.</center></html>";
		assertEquals("The description should be: "
				+ "<html><center>School fees.<br>Pay $50.</center></html>", expected, temp.descriptor(11));
		
		expected = "<html><center>You inherit $100.</center></html>";
		assertEquals("The description should be: "
				+ "<html><center>You inherit $100.</center></html>", expected, temp.descriptor(15));
		
		expected = "<html><center>Bank error in your favor.<br>Collect $200.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(1));
		expected = "<html><center>Doctor's fees.<br>Pay $50.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(2));
		expected = "<html><center>From sale of stock you get $50.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(3));
		expected = "<html><center>Get out of Jail Free.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(4));
		expected = "<html><center>Go directly to Jail.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(5));
		expected = "<html><center>Grand Opera Night.<br>Collect $50 from every player<br>for opening night seats.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(6));
		expected = "<html><center>Holiday Fund matures.<br>Receive $100.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(7));
		expected = "<html><center>Income tax refund.<br>Collect $20.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(8));
		expected = "<html><center>Life insurance matures.<br>Collect $100.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(9));
		expected = "<html><center>Hospital Fees.<br>Pay $50.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(10));
		expected = "<html><center>Receive $25 consultancy fee.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(12));
		expected = "<html><center>You are assessed for street repairs:<br>Pay $40 per house and $115 per hotel you own.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(13));
		expected = "<html><center>You have won second prize in a beauty contest.<br>Collect $10.</center></html>";
		assertEquals("The info is true", expected, temp.descriptor(14));
	}
	
	@Test
	void TitleDeedCardTest() {
		TitleDeedCard temp = new TitleDeedCard();
		
		String expected = "Short Line Railroad";
		assertEquals("The information should be: Short Line Railroad", expected, temp.descriptor(35)[0]);
		
		int expected1 = 75;
		assertEquals("The information should be 75", expected1, temp.descriptor(28)[1]);
		
		expected = "St. Charles Place";
		assertEquals("The information should be: St. Charles Place", expected, temp.descriptor(11)[0]);
		
		expected1 = 625;
		assertEquals("The information should  be 625", expected1, temp.descriptor(13)[5]);
		
		expected = "B & O Railroad";
		assertEquals("The information should be: B & O Railroad", expected, temp.descriptor(25)[0]);
		
		expected1 = 1275;
		assertEquals("The information should be 1275", expected1, temp.descriptor(32)[6]);
		
		expected = "Mediterranean Avenue";
		assertEquals("The information should be: Mediterranean Avenue", expected, temp.descriptor(1)[0]);
		
		expected1 = 800;
		assertEquals("The information should be 800", expected1, temp.descriptor(19)[5]);
		
		expected = "Pacific Avenue";
		assertEquals("The information should be: Pacific Avenue", expected, temp.descriptor(31)[0]);

		assertEquals("The information should be 14", 14, temp.descriptor(16)[1]);
		assertEquals("The information should be 4", 4, temp.descriptor(3)[1]);
		assertEquals("The information should be 25", 25, temp.descriptor(5)[1]);
		assertEquals("The information should be 6", 6, temp.descriptor(6)[1]);
		assertEquals("The information should be 6", 6, temp.descriptor(8)[1]);
		assertEquals("The information should be 8", 8, temp.descriptor(9)[1]);
		assertEquals("The information should be 75", 75, temp.descriptor(12)[1]);
		assertEquals("The information should be 12", 12, temp.descriptor(14)[1]);
		assertEquals("The information should be 25", 25, temp.descriptor(15)[1]);
		assertEquals("The information should be 14", 14, temp.descriptor(18)[1]);
		assertEquals("The information should be 18", 18, temp.descriptor(21)[1]);
		assertEquals("The information should be 18", 18, temp.descriptor(23)[1]);
		assertEquals("The information should be 20", 20, temp.descriptor(24)[1]);
		assertEquals("The information should be 22", 22, temp.descriptor(26)[1]);
		assertEquals("The information should be 22", 22, temp.descriptor(27)[1]);
		assertEquals("The information should be 24", 24, temp.descriptor(29)[1]);
		assertEquals("The information should be 28", 28, temp.descriptor(34)[1]);
		assertEquals("The information should be 35", 35, temp.descriptor(37)[1]);
		assertEquals("The information should be 50", 50, temp.descriptor(39)[1]);
		
		
		
	}

}
