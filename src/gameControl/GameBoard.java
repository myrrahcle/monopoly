package gameControl;

import java.awt.Color;
import java.util.ArrayList;

import blocks.Block;
import blocks.CardPoolBlock;
import blocks.Property;
import blocks.LandProperty;
import cards.TitleDeedCard;

/**
 * Game board
 * 
 * @author Rui
 *
 */
public class GameBoard {
	private ArrayList<Block> blocks;

	/**
	 * Color group
	 * 
	 * @author Rui
	 *
	 */
	public enum ColorGroup {
		BLACK(Color.BLACK, -1), WHITE(Color.WHITE, 0), PURPLE(new Color(88, 9, 55), 1),
		LIGHT_BLUE(new Color(134, 164, 212), 2), PINK(new Color(236, 54, 119), 3), ORANGE(new Color(243, 126, 33), 4),
		RED(new Color(236, 56, 35), 5), YELLOW(new Color(249, 230, 6), 6), GREEN(new Color(17, 164, 90), 7),
		DARK_BLUE(new Color(32, 77, 161), 8);

		private Color color;
		private int id;

		private ColorGroup(Color color, int id) {
			this.color = color;
			this.id = id;
		}

		/**
		 * Get the color of the color group.
		 * 
		 * @return color
		 */
		public Color getColor() {
			return color;
		}

		/**
		 * Get the id of the color group.
		 * 
		 * @return id
		 */
		public int getId() {
			return id;
		}
	}

	/**
	 * Type of buildings.
	 * 
	 * @author Rui
	 *
	 */
	public enum BuildingType {
		HOUSE, HOTEL
	}

	public GameBoard() {
		this.blocks = new ArrayList<>();
		setupBoard();
	}

	/**
	 * Game board setup.
	 */
	private void setupBoard() {
		for (int pos = 0; pos < 40; pos++) {
			LandProperty land = new LandProperty();
			CardPoolBlock cardBlock = new CardPoolBlock();
			Property property = new Property();
			Block block = new Block();
			switch (pos) {
			case 0:
				block.setName("Go");
				block.setPosition(pos);
				break;
			case 10:
				block.setName("Jail Visit");
				block.setPosition(pos);
				break;
			case 20:
				block.setName("Free Parking");
				block.setPosition(pos);
				break;
			case 30:
				block.setName("Go to Jail");
				block.setPosition(pos);
				break;
			case 2:
			case 17:
			case 33:
				cardBlock.setName("Community Chest");
				cardBlock.setType(CardPoolBlock.CardType.COMMUNITYCHEST);
				cardBlock.setPosition(pos);
				block = cardBlock;
				break;
			case 4:
			case 38:
				block.setName("Tax");
				block.setPosition(pos);
				break;
			case 5:
			case 15:
			case 25:
			case 35:
				setupProperty(property, pos);
				block = property;
				break;
			case 7:
			case 22:
			case 36:
				cardBlock.setName("Chance");
				cardBlock.setType(CardPoolBlock.CardType.CHANCE);
				cardBlock.setPosition(pos);
				block = cardBlock;
				break;
			case 12:
			case 28:
				setupProperty(property, pos);
				block = property;
				break;
			case 1:
			case 3:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.PURPLE);
				block = land;
				break;
			case 6:
			case 8:
			case 9:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.LIGHT_BLUE);
				block = land;
				break;
			case 11:
			case 13:
			case 14:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.PINK);
				block = land;
				break;
			case 16:
			case 18:
			case 19:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.ORANGE);
				block = land;
				break;
			case 21:
			case 23:
			case 24:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.RED);
				block = land;
				break;
			case 26:
			case 27:
			case 29:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.YELLOW);
				block = land;
				break;
			case 31:
			case 32:
			case 34:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.GREEN);
				block = land;
				break;
			case 37:
			case 39:
				setupProperty(land, pos);
				land.setColorGroup(ColorGroup.DARK_BLUE);
				block = land;
				break;
			}
			this.blocks.add(pos, block);
		}
	}

	/**
	 * Set up a property that is specified with a Title Deed card.
	 * 
	 * @param property
	 * @param pos      position of the property
	 */
	private void setupProperty(Property property, int pos) {
		Object[] description = TitleDeedCard.descriptor(pos);
		property.setName((String) description[0]);
		property.setPosition(pos);
		property.setMortgage((int) description[description.length - 1]);
		if (property instanceof LandProperty)
			((LandProperty) property).setHousePrice((int) description[description.length - 2]);
	}

	/**
	 * Get all the blocks on the board.
	 * 
	 * @return blocks
	 */
	public ArrayList<Block> getBlocks() {
		return blocks;
	}

	/**
	 * Get the file name of the icon of the block.
	 * 
	 * @param pos position of the player
	 * @return file name
	 */
	public static String getBlockIconName(int pos) {
		String name = "";
		switch (pos) {
		case 0:
			name = "go";
			break;
		case 2:
		case 17:
		case 33:
			name = "communityChest";
			break;
		case 4:
		case 38:
			name = "tax";
			break;
		case 5:
		case 15:
		case 25:
		case 35:
			name = "railroad";
			break;
		case 7:
		case 22:
		case 36:
			name = "chance";
			break;
		case 10:
			name = "jail";
			break;
		case 12:
			name = "electricCompany";
			break;
		case 20:
			name = "freeParking";
			break;
		case 28:
			name = "waterWorks";
			break;
		case 30:
			name = "goToJail";
			break;
		}
		return name;
	}

}
