package gameControl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import blocks.Block;
import blocks.CardPool;
import blocks.CardPoolBlock;
import blocks.CardPoolBlock.CardType;
import blocks.LandProperty;
import blocks.Property;
import cards.TitleDeedCard;
import gameControl.GameBoard.BuildingType;
import gameControl.GameBoard.ColorGroup;
import players.GamePlayer;

/**
 * Game control & main logic.
 * 
 * @author Rui
 *
 */
public class GameControl {
	private static final String[] CHARACTERS = { "Karen Aijou", "Hikari Kagura", "Mahiru Tsuyuzaki", "Junna Hoshimi",
			"Nana Daiba", "Maya Tendou", "Claudine Saijou", "Futaba Isurugi", "Kaoruko Hanayagi" };
	private static final Color[] COLORS = { new Color(243, 85, 92), new Color(98, 146, 233), new Color(97, 191, 153),
			new Color(149, 202, 238), new Color(253, 209, 98), new Color(203, 198, 204), new Color(254, 153, 82),
			new Color(140, 103, 170), new Color(224, 134, 150) };

	private GameBoard board;

	private ArrayList<GamePlayer> players;
	private ArrayList<Integer> retiredId;
	private ArrayList<Block> blocks;
	private int numOfPlayers, numOfHouses, numOfHotels;
	private int result1, result2, rollResult, cardId, currentPos;
	private boolean hasBidFinished, hasChanceFreeCard, hasCCFreeCard;

	public GameControl() {
		this.board = new GameBoard();
		this.players = new ArrayList<>();
		this.retiredId = new ArrayList<>();
		this.blocks = new ArrayList<>();
		this.numOfPlayers = 0;
		this.numOfHouses = 32;
		this.numOfHotels = 12;
		this.rollResult = 0;
		this.cardId = -1;
		this.currentPos = 0;
		this.hasBidFinished = false;
		this.hasChanceFreeCard = false;
		this.hasCCFreeCard = false;
		setupBoard();
		CardPool.shuffle();
	}

	/**
	 * Players setup.
	 * 
	 * @param num number of players
	 */
	public void setUpPlayers(int num) {
		this.numOfPlayers = num;
		for (int i = 0; i < num; i++) {
			GamePlayer player = new GamePlayer();
			player.setId(i);
			player.setName(GameControl.CHARACTERS[i]);
			player.setColor(GameControl.COLORS[i]);
			this.players.add(i, player);
		}
	}

	/**
	 * Game board setup.
	 * <p>
	 * Get the blocks from game board.
	 */
	private void setupBoard() {
		this.blocks = board.getBlocks();
	}

	/**
	 * Get the list of the players.
	 * 
	 * @return players
	 */
	public ArrayList<GamePlayer> getPlayers() {
		return players;
	}

	/**
	 * Get the list of the blocks.
	 * 
	 * @return blocks
	 */
	public ArrayList<Block> getBlocks() {
		return blocks;
	}

	/**
	 * Get all the properties that are specified with Title Deed cards.
	 * 
	 * @return properties
	 */
	public ArrayList<Property> getProperties() {
		ArrayList<Property> properties = new ArrayList<>();
		for (Block b : blocks) {
			if (b instanceof Property)
				properties.add((Property) b);
		}
		return properties;
	}

	/**
	 * Get all the players on a specified block.
	 * 
	 * @param pos position of the block
	 * @return list of players
	 */
	public ArrayList<GamePlayer> getPlayersOnBlock(int pos) {
		ArrayList<GamePlayer> players = new ArrayList<>();
		for (GamePlayer player : this.players) {
			if (player.getPosition() == pos)
				players.add(player);
		}
		return players;
	}

	/**
	 * Get the lands in the specified color group.
	 * 
	 * @param color color group
	 * @return lands
	 */
	public ArrayList<LandProperty> getLandsInGroup(ColorGroup color) {
		ArrayList<LandProperty> lands = new ArrayList<>();
		for (Block b : blocks) {
			if (b instanceof LandProperty && ((LandProperty) b).getColorGroup() == color)
				lands.add((LandProperty) b);
		}
		return lands;
	}

	/**
	 * Roll the dices.
	 * <p>
	 * Check the imprisoning state of the player.
	 * 
	 * @param player
	 * @return result
	 */
	public int roll(GamePlayer player) {
		result1 = (int) (1 + Math.floor(Math.random() * 6));
		result2 = (int) (1 + Math.floor(Math.random() * 6));
		this.rollResult = result1 + result2;
		checkPlayerLegalStatus(player);
		endTurn(blocks.get(player.getPosition()), player);
		return rollResult;
	}

	/**
	 * Get the results of the latest roll.
	 * 
	 * @return results of the two dices
	 */
	public int[] getResults() {
		return new int[] { result1, result2 };
	}

	/**
	 * Determine whether the player can end the current turn.
	 * 
	 * @param block  block at the position of the player
	 * @param player
	 * @return true if the player is able to end the turn
	 */
	@SuppressWarnings("unlikely-arg-type")
	public boolean endTurn(Block block, GamePlayer player) {
		if (checkPlayerBankStatus(block.getPosition(), player) == 0)
			return false;
		else if (checkPlayerBankStatus(block.getPosition(), player) < 0) {
			player.retire(true);
			players.set(player.getId(), player);
			retiredId.add(player.getId());
			return true;
		}
		if (block instanceof Property) {
			if (((Property) block).isOwned() && ((Property) block).getOwner() != player) {
				if (player.hasPaidRent())
					return true;
				else
					rollResult = -1;
			} else if (!((Property) block).isOwned() && !hasBidFinished) {
				rollResult = -1;
			} else
				return true;
		} else if (block instanceof CardPoolBlock) {
			if (cardId >= 0) {
				if (((CardPoolBlock) block).getType() == CardType.CHANCE) {
					if (Arrays.asList(new int[] { 1, 2, 3, 4, 7, 11 }).contains(cardId)) {
						rollResult = -1;
					} else
						return true;
				} else
					return true;
			} else if (cardId < 0 && player.hasDrawnCard())
				return true;
			else {
				rollResult = -1;
			}
		} else
			return true;
		return (rollResult > 0 || player.isImprisoned());
	}

	/**
	 * Initiate the parameters for a new turn.
	 */
	public void newTurn() {
		rollResult = 0;
		cardId = -1;
	}

	/**
	 * Get the result of the latest roll.
	 * 
	 * @return result
	 */
	public int getRollResult() {
		return rollResult;
	}

	/*
	 * Determine whether the bidding has finished.
	 */
	public boolean hasBidFinished() {
		return hasBidFinished;
	}

	/**
	 * Reset the flag for bidding.
	 */
	public void resetBidFlag() {
		hasBidFinished = false;
	}

	/**
	 * Set the latest bid result.
	 * 
	 * @param playerId
	 * @param price
	 */
	public void setBidResult(int playerId, int price) {
		if (playerId < 0) {
			hasBidFinished = true;
			return;
		}
		Property property = (Property) blocks.get(currentPos);
		GamePlayer player = players.get(playerId);
		Object[] des = TitleDeedCard.descriptor(property.getPosition());
		player.buyProperty(property, price);
		property.setOwner(player);
		int rent = (int) des[1];
		if (property.getPosition() % 5 == 0) {
			int numOfStations = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0)
					numOfStations++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0) {
					p.setRent((int) des[numOfStations]);
					blocks.set(p.getPosition(), p);
				}
			}
			return;
		} else if (property instanceof LandProperty) {
			ArrayList<LandProperty> lands = getLandsInGroup(((LandProperty) property).getColorGroup());
			if (player.getProperties().containsAll(lands)) {
				rent *= 2;
				for (LandProperty land : lands) {
					if (land.getBuildings().size() == 0) {
						land.setRent(rent);
						blocks.set(land.getPosition(), land);
					}
				}
				return;
			}
			property.setRent(rent);
		} else {
			int numOfUtilities = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28)
					numOfUtilities++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28) {
					p.setRent((numOfUtilities == 2 ? 10 : 4));
					blocks.set(p.getPosition(), p);
				}
			}
			return;
		}
		blocks.set(property.getPosition(), property);
		hasBidFinished = true;
	}

	/**
	 * Check the bank status of the player
	 * 
	 * @param player
	 */
	public int checkPlayerBankStatus(int position, GamePlayer player) {
		Block block = blocks.get(position);
		if (block instanceof Property && ((Property) block).isOwned()) {
			if (((Property) block).getOwner() != player) {
				if (player.getBalance() < ((Property) block).getRent()) {
					if (player.getPropertyValue() + player.getBalance() < ((Property) block).getRent())
						return -1;
					else
						return 0;
				} else
					return 1;
			} else {
				if (player.getBalance() < 0) {
					if (player.getPropertyValue() + player.getBalance() < 0)
						return -1;
					else
						return 0;
				} else
					return 1;
			}
		} else if (block instanceof CardPoolBlock) {
			if (player.getBalance() <= 0) {
				if (player.getPropertyValue() + player.getBalance() < 0)
					return -1;
				else
					return 0;
			} else
				return 1;
		} else {
			if (player.getBalance() <= 50) {
				if (player.getPropertyValue() + player.getBalance() < 50)
					return -1;
				else
					return 0;
			} else
				return 1;
		}
	}

	public boolean isRetired(int id) {
		return retiredId.contains(id);
	}

	public boolean isGameOver() {
		return retiredId.size() == 2;
	}

	/**
	 * Check the imprisoning state of a player.
	 * 
	 * @param player
	 */
	public void checkPlayerLegalStatus(GamePlayer player) {
		int[] rolls = player.getLastTwoRolls();
		if (!player.isImprisoned()) {
			if (rolls[0] % 2 == 0 && rolls[1] % 2 == 0 && rollResult % 2 == 0) {
				player.changeImprisonState();
				player.moveTo(10);
			}
		} else {
			if (!player.hasPaidFine()) {
				if (rolls[0] % 2 == 0 && rolls[1] % 2 == 0 && rollResult % 2 == 0) {
					player.changeImprisonState();
				} else {
					player.setLastRoll(rollResult);
					rollResult = -1;
				}
			} else {
				if (rolls[0] > 0 && rolls[1] > 0) {
					if (rolls[0] % 2 == 0 || rolls[1] % 2 == 0) {
						player.changeImprisonState();
					} else {
						player.payFine();
						player.income(-50);
						player.changeImprisonState();
					}
				} else {
					player.setLastRoll(rollResult);
					rollResult = -1;
				}
			}
		}
		if (!player.isImprisoned() && player.getPosition() + rollResult > 39)
			player.income(200);
	}

	/**
	 * Set the current position being displayed.
	 * 
	 * @param pos current position
	 */
	public void setCurrentPos(int pos) {
		this.currentPos = pos;
	}

	/**
	 * Get the current position being displayed.
	 * 
	 * @return current position
	 */
	public int getCurrentPos() {
		return currentPos;
	}

	/**
	 * Operations of a player on a property that can be traded.
	 * <p>
	 * Buy property/house, pay rent.
	 * <p>
	 * Update the rent of the property if new transaction is made.
	 * 
	 * @param property property that is specified by a Title Deed card
	 * @param player
	 */
	public void onProperty(Property property, GamePlayer player) {
		Object[] des = TitleDeedCard.descriptor(property.getPosition());
		if (property.isOwned()) {
			if (property.getOwner() == player) {
				if (property instanceof LandProperty) {
					ArrayList<LandProperty> lands = getLandsInGroup(((LandProperty) property).getColorGroup());
					if (player.getProperties().containsAll(lands)) {
						int numOfBuildings = lands.get(0).getBuildings().size();
						int index = 0;
						int i = 0;
						for (LandProperty land : lands) {
							if (land.hasHotel()) {
								i++;
								continue;
							}
							if (numOfBuildings >= land.getBuildings().size()) {
								if (numOfBuildings == land.getBuildings().size()) {
									index = (lands.get(index).getRent() > land.getRent()) ? index : i;
								} else
									index = i;
							}
							numOfBuildings = lands.get(index).getBuildings().size();
							i++;
						}
						LandProperty land = lands.get(index);
						numOfBuildings = land.getBuildings().size();
						if (numOfHotels > 0 && numOfBuildings > 3) {
							land.buildHotel();
							land.setRent((int) des[des.length - 3]);
							numOfHotels--;
							numOfHouses += 4;
							player.income((-1) * land.getHousePrice());
						} else if (numOfHouses > 0 && numOfBuildings <= 3 && !land.hasHotel()) {
							land.buildHouse();
							land.setRent((int) des[numOfBuildings + 2]);
							numOfHouses--;
							player.income((-1) * land.getHousePrice());
						}
						blocks.set(land.getPosition(), land);
					}
				}

			} else {
				if (!player.hasPaidRent()) {
					int rent = property.getRent();
					if (property.getPosition() == 12 || property.getPosition() == 28)
						rent *= player.getLastTwoRolls()[1];
					player.income((-1) * rent);
					player.payRent();
					GamePlayer p = property.getOwner();
					p.income(rent);
					players.set(p.getId(), p);
				}
			}

		} else
			buyProperty(property, player);
	}

	/**
	 * Buy property.
	 * 
	 * @param property
	 * @param player
	 */
	public void buyProperty(Property property, GamePlayer player) {
		Object[] des = TitleDeedCard.descriptor(property.getPosition());
		player.buyProperty(property, property.getMortgage() * 2);
		property.setOwner(player);
		int rent = (int) des[1];
		if (property.getPosition() % 5 == 0) {
			int numOfStations = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0)
					numOfStations++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0) {
					p.setRent((int) des[numOfStations]);
					blocks.set(p.getPosition(), p);
				}
			}
			return;
		} else if (property instanceof LandProperty) {
			ArrayList<LandProperty> lands = getLandsInGroup(((LandProperty) property).getColorGroup());
			if (player.getProperties().containsAll(lands)) {
				rent *= 2;
				for (LandProperty land : lands) {
					if (land.getBuildings().size() == 0) {
						land.setRent(rent);
						blocks.set(land.getPosition(), land);
					}
				}
				return;
			}
			property.setRent(rent);
		} else {
			int numOfUtilities = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28)
					numOfUtilities++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28) {
					p.setRent((numOfUtilities == 2 ? 10 : 4));
					blocks.set(p.getPosition(), p);
				}
			}
			return;
		}
		blocks.set(property.getPosition(), property);
	}

	/**
	 * Sell property.
	 * 
	 * @param property
	 * @param player
	 */
	public void sellProperty(Property property, GamePlayer player) {
		Object[] des = TitleDeedCard.descriptor(property.getPosition());
		player.sellProperty(property);
		property.setOwner(null);
		int rent = (int) des[1];
		if (property.getPosition() % 5 == 0) {
			int numOfStations = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0)
					numOfStations++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() % 5 == 0) {
					p.setRent((int) des[numOfStations]);
					blocks.set(p.getPosition(), p);
				}
			}
		} else if (property instanceof LandProperty) {
			ArrayList<LandProperty> lands = getLandsInGroup(((LandProperty) property).getColorGroup());
			if (lands.size() - 1 == player.getPropertiesInGroup(((LandProperty) property).getColorGroup()).size()) {
				for (LandProperty land : lands) {
					if (land.getBuildings().size() == 0) {
						land.setRent(rent);
						blocks.set(land.getPosition(), land);
					}
				}
			}
			if (!((LandProperty) property).getBuildings().isEmpty()) {
				int refund = 0;
				if (((LandProperty) property).hasHotel())
					refund = 5 * ((LandProperty) property).getHousePrice() / 2;
				else
					refund = ((LandProperty) property).getBuildings().size() * ((LandProperty) property).getHousePrice()
							/ 2;
				for (int i = 0; i < ((LandProperty) property).getBuildings().size(); i++)
					((LandProperty) property).sellHouse();
				player.income(refund);
			}
		} else {
			int numOfUtilities = 1;
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28)
					numOfUtilities++;
			}
			for (Property p : player.getProperties()) {
				if (p.getPosition() == 12 || p.getPosition() == 28) {
					p.setRent((numOfUtilities == 2 ? 10 : 4));
					blocks.set(p.getPosition(), p);
				}
			}
		}
		blocks.set(property.getPosition(), property);
	}

	/**
	 * Determine the methods of getting out of jail for a player.
	 * 
	 * @param player
	 * @return negative values for paying fine/use Free card;
	 *         <p>
	 *         positive values for player id where Free cards are available to sell
	 */
	public int getOutOfJail(GamePlayer player) {
		if (player.hasFreeCard())
			if (player.isFreeCardLocked())
				return -2;
			else
				return -1;
		else {
			for (GamePlayer p : players) {
				if (p.hasFreeCard() && !p.isFreeCardLocked())
					return p.getId();
			}
			return -2;
		}
	}

	/**
	 * Update the info of the player after operations taken in jail.
	 * 
	 * @param player
	 * @param negative/positive values returned by {@link getOutOfJail}
	 */
	public void actionInJail(GamePlayer player, int action) {
		switch (action) {
		case -1:
			int freeCardId = player.useFreeCard();
			player.changeImprisonState();
			if (freeCardId == 4) {
				hasCCFreeCard = false;
				CardPool.returnCCFreeCard();
			} else if (freeCardId == 6) {
				hasChanceFreeCard = false;
				CardPool.returnChanceFreeCard();
			}
			break;
		case -2:
			int[] lastTwoRolls = player.getLastTwoRolls();
			if ((!player.hasPaidFine() && rollResult <= 0 && (lastTwoRolls[0] < 0 || lastTwoRolls[1] < 0))) {
				player.payFine();
				player.income(-50);
			} else if (player.hasPaidFine() && (lastTwoRolls[0] % 2 != 0 && lastTwoRolls[1] % 2 != 0
					&& lastTwoRolls[0] > 0 && lastTwoRolls[1] > 0 && rollResult % 2 != 0)) {
				player.income(-50);
				player.changeImprisonState();
			}
			break;
		default:
			GamePlayer p = players.get(action);
			freeCardId = p.useFreeCard();
			players.set(action, p);

			player.obtainFreeCard(freeCardId);
			player.useFreeCard();
			player.changeImprisonState();
			player.income(-200);

			if (freeCardId == 4) {
				hasCCFreeCard = false;
				CardPool.returnCCFreeCard();
			} else if (freeCardId == 6) {
				hasChanceFreeCard = false;
				CardPool.returnChanceFreeCard();
			}
			break;
		}
	}

	/**
	 * Draw card from stacks of Chance/Community Chests.
	 * 
	 * @param player
	 * @param pos    position of the player
	 */
	public void drawCard(GamePlayer player, int pos) {
		switch (pos) {
		case 2:
		case 17:
		case 33:
			cardId = CardPool.drawCCCard();
			if (!hasCCFreeCard && cardId == 4) {
				hasCCFreeCard = true;
				CardPool.removeCCFreeCard();
			}
			communityChestCards(player, cardId);
			player.drawCard(cardId);
			break;
		case 7:
		case 22:
		case 36:
			cardId = CardPool.drawChanceCard();
			if (!hasChanceFreeCard && cardId == 6) {
				hasChanceFreeCard = true;
				CardPool.removeChanceFreeCard();
			}
			chanceCards(player, cardId);
			player.drawCard(cardId);
		}
	}

	/**
	 * Get the id of the latest drawn card.
	 * 
	 * @return id
	 */
	public int getCardId() {
		return cardId;
	}

	/**
	 * Update the info of the player based on its position.
	 * 
	 * @param player
	 * @param pos    position of the player
	 */
	public void actionsOnBlocks(GamePlayer player, int pos) {
		switch (pos) {
		case 4:
			int tax = (int) ((player.getBalance() * 0.1 > 200) ? player.getBalance() * -0.1 : -200);
			player.income(tax);
			break;
		case 38:
			player.income(-75);
			break;
		case 30:
			player.changeImprisonState();
			player.moveTo(10);
			break;
		}
	}

	/**
	 * Effects of the Chance cards.
	 * 
	 * @param player
	 * @param id     card id
	 */
	public void chanceCards(GamePlayer player, int id) {
		int pos = player.getPosition();
		switch (id) {
		case 0:
			player.moveTo(0);
			player.income(200);
			break;
		case 1:
			player.moveTo(29);
			if (pos > 29)
				player.income(200);
			break;
		case 2:
			player.moveTo(16);
			if (pos > 16)
				player.income(200);
			break;
		case 3:
			if (pos >= 0 && pos < 20)
				player.moveTo(12);
			else if (pos >= 20 && pos < 40)
				player.moveTo(28);
			break;
		case 4:
			if (pos >= 0 && pos < 10)
				player.moveTo(5);
			else if (pos >= 10 && pos < 20)
				player.moveTo(15);
			else if (pos >= 20 && pos < 30)
				player.moveTo(25);
			else if (pos >= 30 && pos < 40)
				player.moveTo(35);
			break;
		case 5:
			player.income(50);
			break;
		case 6:
			player.obtainFreeCard(6);
			break;
		case 7:
			player.moveTo(pos - 3);
			actionsOnBlocks(player, pos - 3);
			break;
		case 8:
			player.changeImprisonState();
			player.moveTo(10);
			break;
		case 9:
			int fee = 0;
			for (Property p : player.getProperties()) {
				if (p instanceof LandProperty) {
					for (BuildingType h : ((LandProperty) p).getBuildings()) {
						if (h == BuildingType.HOTEL)
							fee += 100;
						else
							fee += 25;
					}
				}
			}
			player.income((-1) * fee);
			break;
		case 10:
			player.income(-15);
			break;
		case 11:
			player.moveTo(5);
			if (pos > 5)
				player.income(200);
			break;
		case 12:
			player.moveTo(20);
			if (pos > 20)
				player.income(200);
			break;
		case 13:
			player.income(-50 * (numOfPlayers - 1));
			for (GamePlayer p : players) {
				if (p != player) {
					p.income(50);
					players.set(p.getId(), p);
				}
			}
			break;
		case 14:
			player.income(50);
			break;
		case 15:
			player.income(150);
			break;
		}
	}

	/**
	 * Effects of the Community Chest cards.
	 * 
	 * @param player
	 * @param id     card id
	 */
	public void communityChestCards(GamePlayer player, int id) {
		switch (id) {
		case 0:
			player.moveTo(0);
			player.income(200);
			break;
		case 1:
			player.income(200);
			break;
		case 2:
			player.income(-50);
			break;
		case 3:
			player.income(50);
			break;
		case 4:
			player.obtainFreeCard(4);
			break;
		case 5:
			player.changeImprisonState();
			player.moveTo(10);
		case 6:
			player.income(50 * (numOfPlayers - 1));
			for (GamePlayer p : players) {
				if (p != player) {
					p.income(-50);
					players.set(p.getId(), p);
				}
			}
			break;
		case 7:
			player.income(100);
			break;
		case 8:
			player.income(20);
			break;
		case 9:
			player.income(100);
			break;
		case 10:
			player.income(-50);
			break;
		case 11:
			player.income(-50);
			break;
		case 12:
			player.income(25);
			break;
		case 13:
			int fee = 0;
			for (Property p : player.getProperties()) {
				if (p instanceof LandProperty) {
					for (BuildingType h : ((LandProperty) p).getBuildings()) {
						if (h == BuildingType.HOTEL)
							fee += 115;
						else
							fee += 40;
					}
				}
			}
			player.income((-1) * fee);
			break;
		case 14:
			player.income(10);
			break;
		case 15:
			player.income(100);
			break;
		}
	}

}
